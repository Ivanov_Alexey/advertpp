<?php

namespace Application\Migrations;

use AppBundle\Entity\Setting;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * New system settings
 */
class Version20170809064730 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("
          INSERT INTO `settings` (`name`, `display_name`, `type`, `value`, `common`) VALUES 
            ('mail_from', 'Адрес отправителя почты', ".Setting::TYPE_TEXT.", 'info@advertpp.ru', 1),
            ('landing_title', 'Title лендинг-страницы', ".Setting::TYPE_TEXT.", 'AdvertPP', 1),
            ('landing_description', 'Description лендинг-страницы', ".Setting::TYPE_TEXT.", 'AdvertPP', 1),
            ('landing_keywords', 'Keywords лендинг-страницы', ".Setting::TYPE_TEXT.", 'AdvertPP', 1),
            ('admin_message', 'Текст сообщения от администратора всем пользователям системы', ".Setting::TYPE_TEXT.", '', 1),
            ('admin_message_show', 'Включение сообщения от администратора системы', ".Setting::TYPE_BOOLEAN.", '0', 1)
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("DELETE FROM `settings` WHERE `name`='mail_from';");
        $this->addSql("DELETE FROM `settings` WHERE `name`='landing_title';");
        $this->addSql("DELETE FROM `settings` WHERE `name`='landing_description';");
        $this->addSql("DELETE FROM `settings` WHERE `name`='landing_keywords';");
        $this->addSql("DELETE FROM `settings` WHERE `name`='admin_message';");
        $this->addSql("DELETE FROM `settings` WHERE `name`='admin_message_show';");
    }
}
