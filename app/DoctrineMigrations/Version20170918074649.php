<?php

namespace Application\Migrations;

use AppBundle\Entity\Setting;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170918074649 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("
          INSERT INTO `settings` (`name`, `display_name`, `type`, `value`, `common`) VALUES 
            ('like_commission', 'Комиссия при продаже лайка (%)', ".Setting::TYPE_NUMBER.", '5', 1),
            ('withdraw_commission_yandex', 'Комиссия при выводе средств на Yandex.Money (%)', ".Setting::TYPE_NUMBER.", '0', 1),
            ('withdraw_commission_webmoney', 'Комиссия при выводе средств на WebMoney (%)', ".Setting::TYPE_NUMBER.", '0', 1),
            ('withdraw_commission_qiwi', 'Комиссия при выводе средств на QIWI (%)', ".Setting::TYPE_NUMBER.", '0', 1),
            ('withdraw_commission_card', 'Комиссия при выводе средств на карту (%)', ".Setting::TYPE_NUMBER.", '0', 1)
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("DELETE FROM `settings` WHERE `name`='like_commission';");
        $this->addSql("DELETE FROM `settings` WHERE `name`='withdraw_commission_yandex';");
        $this->addSql("DELETE FROM `settings` WHERE `name`='withdraw_commission_webmoney';");
        $this->addSql("DELETE FROM `settings` WHERE `name`='withdraw_commission_qiwi';");
        $this->addSql("DELETE FROM `settings` WHERE `name`='withdraw_commission_card';");
    }
}
