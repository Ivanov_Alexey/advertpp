<?php

namespace AppBundle\Command;


use AppBundle\Entity\Order;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProcessOrdersCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('orders:process');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        
        $orders = $em->getRepository('AppBundle:Order')
            ->findExpired();
        foreach ($orders as $order) {
            $output->writeln('Expiring order #'.$order->getId());
            if ($this->getContainer()->get('finance')->processOrder($order, Order::STATUS_EXPIRED)) {
                $this->sendExpiredMail($order);
            }
        }
        $em->flush();
    }

    /**
     * @param Order $order
     * @throws \Twig_Error
     */
    private function sendExpiredMail($order)
    {
        $message = new \Swift_Message('Автоматическая регистрация');
        $message
            ->setFrom($this->getContainer()->get('settings')->get('mail_from'))
            ->setTo($order->getClient()->getEmail())
            ->setBody(
                $this->getContainer()->get('templating')->render('@App/mail/order_expired.html.twig', array(
                    'doer_name' => $order->getDoerGoogleAccount()->getChannelName(),
                )),
                'text/html'
            );
        $this->getContainer()->get('mailer')->send($message);
    }
}