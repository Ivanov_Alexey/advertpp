<?php

namespace AppBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProcessPaymentsCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('payments:process');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $expired = $em->getRepository('AppBundle:Deposit')
            ->findExpired();
        
        foreach ($expired as $deposit) {
            $this->getContainer()->get('finance')->expireDeposit($deposit);
            $output->writeln('Deposit '.$deposit->getId().' expired.');
        }
    }
}