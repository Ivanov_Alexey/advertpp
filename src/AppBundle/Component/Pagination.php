<?php

namespace AppBundle\Component;

use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;

class Pagination
{
    protected $options = array(
        'per_page' => 10,
        'controls_width' => 2,
    );

    protected $current_page;

    protected $page_count;

    protected $query;

    /**
     * @var Paginator
     */
    protected $paginator;

    public function __construct($query, $options = array())
    {
        $this->query = $query;
        $this->options = array_merge($this->options, $options);
    }

    /**
     * @param int $page
     * @return array|bool
     */
    public function paginate($page)
    {
        $this->current_page = $page;

        $this->initPaginator();
        $this->paginator->getQuery()
            ->setFirstResult($this->options['per_page'] * ($page - 1))
            ->setMaxResults($this->options['per_page']);

        return $this->paginator;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param array $inherit_params
     * @return array
     */
    public function getControls($request, $inherit_params = array())
    {
        $this->initPaginator();

        $inherit_params_values = array();
        foreach ($inherit_params as $key) {
            $inherit_params_values[$key] = $request->get($key);
        }

        return self::generateControls(
            $request->get('_route'),
            $inherit_params_values,
            $this->page_count,
            $this->options['controls_width'],
            $this->current_page
        );
    }

    /**
     * @return int|number
     */
    public function count()
    {
        $this->initPaginator();

        return $this->paginator->count();
    }

    /**
     * @return int|number
     */
    public function pageCount()
    {
        $this->initPaginator();

        return $this->page_count;
    }

    /**
     * Инициализировать раджинатор
     */
    private function initPaginator()
    {
        if (!$this->paginator) {
            $this->paginator = new Paginator($this->query);
            $this->page_count = ceil($this->paginator->count() / $this->options['per_page']);
        }
    }

    /**
     * Сгенерировать контролы паджинации
     * @param $route
     * @param $params
     * @param $page_count
     * @param int $controls_width
     * @param int $current_page
     * @return array|bool
     */
    static function generateControls($route, $params, $page_count, $controls_width = 2, $current_page = 0)
    {
        // Если страница одна, то не нужно выводить контролы
        if ($page_count <= 1) {
            return false;
        }

        $controls = array();
        if ($current_page > $controls_width * 2) {
            $controls[] = array(
                'route' => $route,
                'params' => array_merge(array('page' => 1), $params),
                'label' => 'Первая',
                'active' => false,
            );
        }

        $first_shown_page = max(1, $current_page - $controls_width);
        $last_shown_page = min($current_page + $controls_width, $page_count);
        for ($i = $first_shown_page; $i <= $last_shown_page; $i++) {
            $controls[] = array(
                'route' => $route,
                'params' => array_merge(array('page' => $i), $params),
                'label' => $i,
                'active' => $i == $current_page,
            );
        }

        if ($last_shown_page < $page_count) {
            $controls[] = array(
                'route' => $route,
                'params' => array_merge(array('page' => $page_count), $params),
                'label' => 'Последняя',
                'active' => false,
            );
        }

        return $controls;
    }
}