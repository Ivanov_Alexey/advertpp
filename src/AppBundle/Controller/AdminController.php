<?php

namespace AppBundle\Controller;

use AppBundle\Form\Type\SettingsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('@App/Admin/index.html.twig');
    }
    
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function settingsAction(Request $request)
    {
        $form = $this->createForm(
            SettingsType::class,
            $this->get('settings')->getAll()
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('settings')->setMany($form->getData());
        }

        return $this->render('@App/Admin/settings.html.twig', array(
            'form' => $form->createView(),
            'keys' => $this->get('settings')->keys(),
        ));
    }
}