<?php

namespace AppBundle\Controller;


use AppBundle\Component\Pagination;
use AppBundle\Entity\Conversation;
use AppBundle\Entity\Message;
use AppBundle\Form\Type\AdminMessageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ConversationAdminController extends Controller
{
    /**
     * @param Request $request
     * @param $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, $page)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        
        $query = $em->getRepository('AppBundle:Conversation')
            ->queryAllOrderByStatusDate();

        $pagination = new Pagination($query, array(
            'per_page' => 20,
        ));
        
        return $this->render('@App/ConversationAdmin/index.html.twig', array(
            'pagination' => $pagination->paginate($page),
            'pagination_controls' => $pagination->getControls($request),
        ));
    }

    /**
     * @param Request $request
     * @param $conversation_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(Request $request, $conversation_id)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $conversation = $em->getRepository('AppBundle:Conversation')
            ->find($conversation_id);
        $message = new Message();
        $form = $this->createForm(AdminMessageType::class, $message);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($message->isClosing()) {
                $conversation->setStatus(Conversation::STATUS_CLOSED);
            }
            $conversation->setUpdatedAt(new \DateTime('now'));
            $message
                ->setConversation($conversation)
                ->setCreatedAt(new \DateTime('now'))
                ->setUser($this->getUser());
            $em->persist($message);
            $em->persist($conversation);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_conversations'));
        }

        return $this->render('@App/ConversationAdmin/view.html.twig', array(
            'conversation' => $conversation,
            'form' => $form->createView(),
        ));
    }
}