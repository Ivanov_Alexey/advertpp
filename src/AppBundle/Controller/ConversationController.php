<?php

namespace AppBundle\Controller;


use AppBundle\Component\Pagination;
use AppBundle\Entity\Conversation;
use AppBundle\Entity\Message;
use AppBundle\Form\Type\ConversationType;
use AppBundle\Form\Type\MessageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ConversationController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function startAction(Request $request)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $conversation = new Conversation();
        $conversation->setStartMessage(new Message());
        $form = $this->createForm(ConversationType::class, $conversation);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $conversation
                ->setCreatedAt(new \DateTime('now'))
                ->setUpdatedAt(new \DateTime('now'))
                ->setStatus(Conversation::STATUS_OPENED)
                ->setStarter($this->getUser());
            $conversation->getStartMessage()
                ->setConversation($conversation)
                ->setUser($this->getUser())
                ->setCreatedAt(new \DateTime('now'));

            $em->persist($conversation);
            $em->persist($conversation->getStartMessage());
            $em->flush();
            
            $this->addFlash('success', 'Ваш запрос успешно отправлен');
            return $this->redirect($this->generateUrl('support_conversations'));
        }

        return $this->render('@App/Conversation/start.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function viewAction(Request $request, $conversation_id)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $conversation = $em->getRepository('AppBundle:Conversation')
            ->find($conversation_id);
        if ($conversation->getStarter()->getId() != $this->getUser()->getId()) {
            throw $this->createAccessDeniedException();
        }
        $message = new Message();
        $form = $this->createForm(MessageType::class, $message);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $conversation->setUpdatedAt(new \DateTime('now'));
            if ($conversation->getStatus() == Conversation::STATUS_CLOSED) {
                $conversation->setStatus(Conversation::STATUS_REOPENED);
            }
            $message
                ->setConversation($conversation)
                ->setCreatedAt(new \DateTime('now'))
                ->setUser($this->getUser());
            $em->persist($message);
            $em->persist($conversation);
            $em->flush();

            return $this->redirect($this->generateUrl('support_conversation_view', array('conversation_id' => $conversation->getId())));
        }

        return $this->render('@App/Conversation/view.html.twig', array(
            'conversation' => $conversation,
            'form' => $form->createView(),
        ));
    }

    /**
     * @param Request $request
     * @param $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request, $page)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('AppBundle:Conversation')
            ->queryAllByUserOrderByUpdated($this->getUser());

        $pagination = new Pagination($query, array(
            'per_page' => 20,
        ));

        return $this->render('@App/Conversation/list.html.twig', array(
            'pagination' => $pagination->paginate($page),
            'pagination_controls' => $pagination->getControls($request),
        ));
    }
}