<?php

namespace AppBundle\Controller;


use AppBundle\Component\Pagination;
use AppBundle\Entity\Deposit;
use AppBundle\Form\Type\DepositManualType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DepositAdminController extends Controller
{
    /**
     * @param Request $request
     * @param $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, $page)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('AppBundle:Deposit')
            ->getQueryAll();

        $pagination = new Pagination($query, array(
            'per_page' => 20,
        ));

        return $this->render('@App/DepositAdmin/index.html.twig', array(
            'pagination' => $pagination->paginate($page),
            'pagination_controls' => $pagination->getControls($request),
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction($id)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $deposit = $em->getRepository('AppBundle:Deposit')
            ->find($id);

        return $this->render('@App/DepositAdmin/view.html.twig', array(
            'deposit' => $deposit,
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processAction(Request $request, $id)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $deposit = $em->getRepository('AppBundle:Deposit')
            ->find($id);

        if ($request->get('action') == 'cancel') {
            if ($this->get('finance')->cancelDeposit($deposit)) {
                $this->addFlash('success', 'Пополнение успешно отменено.');
            } else {
                $this->addFlash('error', 'Не удалось отменить поплонение.');
            }
        } else if ($request->get('action') == 'do') {
            if ($this->get('finance')->confirmDeposit($deposit)) {
                $this->addFlash('success', 'Пополнение успешно выполнено.');
            } else {
                $this->addFlash('error', 'Не удалось выполненить поплонение.');
            }
        }

        return $this->redirect($this->generateUrl('admin_deposit_view', array('id' => $deposit->getId())));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function manualAction(Request $request)
    {
        $deposit = new Deposit();
        $form = $this->createForm(DepositManualType::class, $deposit);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $deposit
                ->setType(Deposit::TYPE_OTHER)
                ->setCreatedAt(new \DateTime('now'));
            if ($this->get('finance')->confirmDeposit($deposit)) {
                return $this->redirect($this->generateUrl('admin_deposit_view', array('id' => $deposit->getId())));
            } else {
                $this->addFlash('error', 'Не удалось выполненить поплонение.');
            }
        }

        return $this->render('@App/DepositAdmin/manual.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}