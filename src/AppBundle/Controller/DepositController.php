<?php

namespace AppBundle\Controller;


use AppBundle\Component\Pagination;
use AppBundle\Entity\Deposit;
use AppBundle\Form\Type\DepositType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class DepositController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction()
    {
        $deposit = new Deposit();
        $form = $this->createForm(DepositType::class, $deposit);

        return $this->render('@App/Deposit/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function saveAction(Request $request)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $deposit = new Deposit();
        $form = $this->createForm(DepositType::class, $deposit);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $deposit
                    ->setUser($this->getUser())
                    ->setStatus(Deposit::STATUS_NEW)
                    ->setCreatedAt(new \DateTime('now'));
                $em->persist($deposit);
                $em->flush();
                return $this->json(array(
                    'status' => 'success',
                    'request' => $this->getDepositRequestParams($deposit),
                ));
            } else {
                $errors = array();
                foreach ($form->getErrors(true) as $error) {
                    $errors[] = $error->getMessage();
                }
                return $this->json(array(
                    'status' => 'error',
                    'error' => implode(', ', $errors),
                ));
            }
        }

        return $this->json(array(
            'status' => 'error',
            'error' => 'Ошибка отправки формы',
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectAction($id)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $deposit = $em->getRepository('AppBundle:Deposit')
            ->find($id);
        if ($deposit instanceof Deposit && $deposit->getStatus() == Deposit::STATUS_NEW) {
            $deposit->setStatus(Deposit::STATUS_PROCESSING);
            $em->persist($em);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('deposit_history'));
    }

    /**
     * @param Request $request
     * @param $page
     * @return Response
     */
    public function historyAction(Request $request, $page)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $deposits_query = $em->getRepository('AppBundle:Deposit')
            ->queryAllByUser($this->getUser());

        $pagination = new Pagination($deposits_query, array(
            'per_page' => 10,
        ));

        return $this->render('@App/Deposit/history.html.twig', array(
            'pagination' => $pagination->paginate($page),
            'pagination_controls' => $pagination->getControls($request),
        ));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function yandexConfirmAction(Request $request)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $sha1 = $request->get('sha1_hash');
        $check_params = array(
            'p2p-incoming',
            $request->get('operation_id'),
            $request->get('amount'),
            $request->get('currency'),
            $request->get('datetime'),
            $request->get('sender'),
            $request->get('codepro'),
            $this->getParameter('yandex_money_secret'),
            $request->get('label'),
        );

        if (sha1(implode('&', $check_params)) == $sha1) {
            $deposit = $em->getRepository('AppBundle:Deposit')
                ->find(substr($request->get('label'), 1));
            if (!$this->get('finance')->confirmDeposit($deposit)) {
                $deposit->setStatus(Deposit::STATUS_FAIL);
                $em->persist($deposit);
                $em->flush();
            }
        }

        return new Response();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function webmoneyResultAction(Request $request)
    {
        if ($request->get('LMI_PREREQUEST') == '1') {
            return new Response('YES');
        }

        if ($request->get('LMI_PAYMENT_NO') !== null) {
            /** @var \Doctrine\ORM\EntityManager $em */
            $em = $this->getDoctrine()->getManager();

            $deposit = $em->getRepository('AppBundle:Deposit')
                ->find($request->get('LMI_PAYMENT_NO'));
            if ($deposit instanceof Deposit) {
                $sha256 = $request->get('LMI_HASH2');
                $check_params = array();
                $check_params[] = $request->get('LMI_PAYEE_PURSE');
                $check_params[] = $request->get('LMI_PAYMENT_AMOUNT');
                if ($request->get('LMI_HOLD') !== null) {
                    $check_params[] = $request->get('LMI_HOLD');
                }
                $check_params[] = $request->get('LMI_PAYMENT_NO');
                $check_params[] = $request->get('LMI_MODE');
                $check_params[] = $request->get('LMI_SYS_INVS_NO');
                $check_params[] = $request->get('LMI_SYS_TRANS_NO');
                $check_params[] = $request->get('LMI_SYS_TRANS_DATE');
                $check_params[] = $this->getParameter('webmoney_secret');
                $check_params[] = $request->get('LMI_PAYER_PURSE');
                $check_params[] = $request->get('LMI_PAYER_WM');
                if ($sha256 == strtoupper(hash('sha256', implode(';', $check_params)))) {
                    if ($deposit->getAmount() != $request->get('LMI_PAYMENT_AMOUNT')) {
                        $deposit->setAmount($request->get('LMI_PAYMENT_AMOUNT'));
                    }
                    $deposit->setStatus(Deposit::STATUS_PROCESSING);
                } else {
                    $deposit->setStatus(Deposit::STATUS_FAIL);
                }
                $em->persist($deposit);
                $em->flush();
            }
        }

        return new Response();
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function webmoneySuccessAction(Request $request)
    {
        if ($request->get('LMI_PAYMENT_NO') !== null) {
            /** @var \Doctrine\ORM\EntityManager $em */
            $em = $this->getDoctrine()->getManager();

            $deposit = $em->getRepository('AppBundle:Deposit')
                ->find($request->get('LMI_PAYMENT_NO'));
            if ($deposit instanceof Deposit && $deposit->getStatus() == Deposit::STATUS_PROCESSING) {
                if (!$this->get('finance')->confirmDeposit($deposit)) {
                    $deposit->setStatus(Deposit::STATUS_FAIL);
                    $em->persist($deposit);
                    $em->flush();
                }
            }
        }
        return $this->redirect($this->generateUrl('deposit_history'));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function webmoneyFailAction(Request $request)
    {
        if ($request->get('LMI_PAYMENT_NO') !== null) {
            /** @var \Doctrine\ORM\EntityManager $em */
            $em = $this->getDoctrine()->getManager();

            $deposit = $em->getRepository('AppBundle:Deposit')
                ->find($request->get('LMI_PAYMENT_NO'));
            if ($deposit instanceof Deposit) {
                $deposit->setStatus(Deposit::STATUS_FAIL);
                $em->persist($deposit);
                $em->flush();
            }
        }
        return $this->redirect($this->generateUrl('deposit_history'));
    }

    /**
     * @param Deposit $deposit
     * @return array
     */
    private function getDepositRequestParams($deposit)
    {
        $result = array();
        switch ($deposit->getType()) {
            case Deposit::TYPE_YANDEX:
                $result['url'] = 'https://money.yandex.ru/quickpay/confirm.xml';
                $result['params'] = array(
                    'receiver' => $this->getParameter('yandex_money_account'),
                    'quickpay-form' => 'shop',
                    'targets' => 'Пополнение баланса AdvertPP',
                    'paymentType' => 'PC',
                    'sum' => $deposit->getAmount(),
                    'label' => '#'.$deposit->getId(),
                    'successURL' => $this->generateUrl('deposit_redirect', array('id' => $deposit->getId()), UrlGeneratorInterface::ABSOLUTE_URL),
                );
                break;
            case Deposit::TYPE_WEBMONEY:
                $result['url'] = 'https://merchant.webmoney.ru/lmi/payment.asp';
                $result['params'] = array(
                    'LMI_PAYEE_PURSE' => $this->getParameter('webmoney_account'),
                    'LMI_PAYMENT_AMOUNT' => $deposit->getAmount(),
                    'LMI_PAYMENT_NO' => $deposit->getId(),
                    'LMI_PAYMENT_DESC_BASE64' => base64_encode('Пополнение баланса AdvertPP'),
                    'LMI_SIM_MODE' => 2, // Режим тестирования
                );
        }
        return $result;
    }
}