<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Faq;
use AppBundle\Form\Type\AdminFaqType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class FaqAdminController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $faqs = $em->getRepository('AppBundle:Faq')
            ->findAll();

        return $this->render('@App/FaqAdmin/index.html.twig', array(
            'faqs' => $faqs,
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $faq = new Faq();
        $form = $this->createForm(AdminFaqType::class, $faq, array('new' => true));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($faq);
            $em->flush();
            $this->addFlash('success', 'Вопрос успешно добавлен');
            return $this->redirect($this->generateUrl('admin_faq'));
        }

        return $this->render('@App/FaqAdmin/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id, Request $request)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $faq = $em->getRepository('AppBundle:Faq')->find($id);
        if (!$faq) {
            throw $this->createNotFoundException('Запись не найдена');
        }
        $form = $this->createForm(AdminFaqType::class, $faq, array('new' => false));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($faq);
            $em->flush();
            $this->addFlash('success', 'Вопрос успешно отредактирован');
            return $this->redirect($this->generateUrl('admin_faq'));
        }

        return $this->render('@App/FaqAdmin/edit.html.twig', array(
            'faq' => $faq,
            'form' => $form->createView(),
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $faq = $em->getRepository('AppBundle:Faq')->find($id);
        if ($faq instanceof Faq) {
            $em->remove($faq);
            $em->flush();
            $this->addFlash('success', 'Вопрос успешно удален');
        } else {
            $this->addFlash('success', 'Не удалось удалить вопрос');
        }

        return $this->redirect($this->generateUrl('admin_faq'));
    }
}