<?php

namespace AppBundle\Controller;


use AppBundle\Component\Pagination;
use AppBundle\Entity\Offer;
use AppBundle\Entity\Order;
use AppBundle\Entity\User;
use AppBundle\Form\Type\OfferType;
use AppBundle\Form\Type\OrderBuyType;
use AppBundle\Form\Type\OrderProcessType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class LikeController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $google_account = $this->get('google')->getCurrentGoogleAccount();

        // Входящие ждущие заявки
        $incoming = $em->getRepository('AppBundle:Order')
            ->findWaitingByDoerGoogleAccount($google_account);
        if ($google_account !== null) {
            // Исходящие ждущие заявки
            $outgoing = $em->getRepository('AppBundle:Order')
                ->findWaitingByClientGoogleAccount($google_account);
            // Все неудачные заявки
            $failed = $em->getRepository('AppBundle:Order')
                ->findFailedByGoogleAccount($google_account);
            // Все удачные заявки
            $succeed = $em->getRepository('AppBundle:Order')
                ->findSucceedByGoogleAccount($google_account);
        } else {
            // Исходящие ждущие заявки
            $outgoing = $em->getRepository('AppBundle:Order')
                ->findWaitingByClient($this->getUser());
            // Все неудачные заявки
            $failed = $em->getRepository('AppBundle:Order')
                ->findFailedByUser($this->getUser());
            // Все удачные заявки
            $succeed = $em->getRepository('AppBundle:Order')
                ->findSucceedByUser($this->getUser());
        }

        return $this->render('@App/Like/index.html.twig', array(
            'incoming' => $incoming,
            'outgoing' => $outgoing,
            'failed' => $failed,
            'succeed' => $succeed,
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function indexUpdateAction(Request $request)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $this->getUser();
        $google_account = $this->get('google')->getCurrentGoogleAccount();

        if (!empty($request->get('display'))) {
            $user->setSettingDashboardDisplay($request->get('display'));
            $em->persist($user);
            $em->flush();
        }

        // Входящие ждущие заявки
        $incoming = $em->getRepository('AppBundle:Order')
            ->findWaitingByDoerGoogleAccount($google_account);
        // Проверка на новые заявки
        $has_new = false;
        if (($last_update = $request->get('last-update')) !== null) {
            foreach ($incoming as $order) {
                if ($order->getCreatedAt()->getTimestamp() > $last_update) {
                    $has_new = true;
                }
            }
        }
        if ($google_account !== null) {
            // Исходящие ждущие заявки
            $outgoing = $em->getRepository('AppBundle:Order')
                ->findWaitingByClientGoogleAccount($google_account);
            // Все неудачные заявки
            $failed = $em->getRepository('AppBundle:Order')
                ->findFailedByGoogleAccount($google_account);
            // Все удачные заявки
            $succeed = $em->getRepository('AppBundle:Order')
                ->findSucceedByGoogleAccount($google_account);
        } else {
            // Исходящие ждущие заявки
            $outgoing = $em->getRepository('AppBundle:Order')
                ->findWaitingByClient($this->getUser());
            // Все неудачные заявки
            $failed = $em->getRepository('AppBundle:Order')
                ->findFailedByUser($this->getUser());
            // Все удачные заявки
            $succeed = $em->getRepository('AppBundle:Order')
                ->findSucceedByUser($this->getUser());
        }

        if ($user->getSettingDashboardDisplay() == User::SETTING_DASHBOARD_DISPLAY_TABLE) {
            $result = array(
                'incoming' => $this->renderView('@App/Like/_orders_incoming_table.html.twig', array(
                    'incoming' => $incoming,
                )),
                'outgoing' => $this->renderView('@App/Like/_orders_outgoing_table.html.twig', array(
                    'outgoing' => $outgoing,
                )),
                'failed' => $this->renderView('@App/Like/_orders_failed_table.html.twig', array(
                    'failed' => $failed,
                )),
                'succeed' => $this->renderView('@App/Like/_orders_succeed_table.html.twig', array(
                    'succeed' => $succeed,
                )),
            );
        } else {
            $result = array(
                'incoming' => $this->renderView('@App/Like/_orders_incoming_tiles.html.twig', array(
                    'incoming' => $incoming,
                )),
                'outgoing' => $this->renderView('@App/Like/_orders_outgoing_tiles.html.twig', array(
                    'outgoing' => $outgoing,
                )),
                'failed' => $this->renderView('@App/Like/_orders_failed_tiles.html.twig', array(
                    'failed' => $failed,
                )),
                'succeed' => $this->renderView('@App/Like/_orders_succeed_tiles.html.twig', array(
                    'succeed' => $succeed,
                )),
            );
        }
        return $this->json(array_merge($result, array('has_new' => $has_new)));
    }
    
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function buyAction(Request $request)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $order = new Order();
        $form = $this->createForm(OrderBuyType::class, $order);
        $form->handleRequest($request);

        $offers = $em->getRepository('AppBundle:Offer')
            ->findAvailableForUser($this->getUser());

        if ($form->isSubmitted() && $form->isValid() && $order->processVideoLink()) {
            $selected_offer = null;
            foreach ($offers as $offer) {
                if ($offer->getId() == $order->getOfferId()) {
                    $selected_offer = $offer;
                    break;
                }
            }
            if ($selected_offer !== null) {
                if ($selected_offer->getFullPrice() <= $this->getUser()->getBalance()->getAmount()) {
                    $order
                        ->setClient($this->getUser())
                        ->setDoer($selected_offer->getGoogleAccount()->getUser())
                        ->setDoerGoogleAccount($selected_offer->getGoogleAccount())
                        ->setPrice($selected_offer->getPrice())
                        ->setFullPrice($selected_offer->getFullPrice())
                        ->setStatus(Order::STATUS_WAITING)
                        ->setCreatedAt(new \DateTime('now'));
                    if (!empty($this->get('google')->getCurrentGoogleAccount())) {
                        $order->setClientGoogleAccount($this->get('google')->getCurrentGoogleAccount());
                    }
                    if ($this->get('finance')->createOrder($order)) {
                        $this->addFlash('success', 'Предложение о покупке успешно размещено');
                        return $this->redirect($this->generateUrl('like_index'));
                    } else {
                        $this->addFlash('error', 'Не удалось разместить предложение о покупке');
                    }
                } else {
                    $form->get('offer_id')
                        ->addError(new \Symfony\Component\Form\FormError('Недостаточно средств для покупки. Полполните баланс.'));
                }
            }
        }

        $my_videos = $this->get('google')->videoList();
        return $this->render('@App/Like/buy.html.twig', array(
            'my_videos' => $my_videos,
            'offers' => $offers,
            'form' => $form->createView(),
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function loadMoreVideosAction(Request $request)
    {
        if (!empty($token = $request->get('token')) && $this->get('google')->isAuthenticated()) {
            $videos = $this->get('google')->videoList($token);
            return $this->json(array(
                'next' => $videos->getNextPageToken(),
                'content' => $this->renderView('@App/Like/_buy_video_list.html.twig', array(
                    'video_items' => $videos->getItems(),
                )),
            ));
        } else {
            throw $this->createNotFoundException();
        }
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function sellAction(Request $request)
    {
        $current_google_account = $this->get('google')
            ->getCurrentGoogleAccount();

        if ($current_google_account === null) {
            return $this->render('@App/Like/sell_blocked.html.twig');
        }

        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $offer = $em->getRepository('AppBundle:Offer')
            ->findOneBy(array('google_account' => $current_google_account));
        if ($offer === null) {
            $offer = new Offer();
            $offer->setGoogleAccount($current_google_account);
        }

        $form = $this->createForm(OfferType::class, $offer);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // Обновляем информацию о канале пользователя для клиентов
            if ($this->get('google')->updateYoutubeChannel()) {
                // Считаем комиссию
                $offer->setFullPrice($this->get('finance')->getFullLikePrice($offer->getPrice()));
                // Сохраняем предложение
                $em->persist($offer);
                $em->flush();

                $this->addFlash('success', 'Предложение успешно размещено');
            } else {
                $this->addFlash('error',
                    'Не удлось обновить статистику канала. Попробуйте <a href="' .
                    $this->generateUrl('google_auth_code')
                    . '">обновить привязку</a>.'
                );
            }
        }

        return $this->render('@App/Like/sell.html.twig', array(
            'offer' => $offer,
            'form' => $form->createView(),
        ));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function cancelSellAction()
    {
        $current_google_account = $this->get('google')
            ->getCurrentGoogleAccount();
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $offer = $em->getRepository('AppBundle:Offer')
            ->findOneBy(array('google_account' => $current_google_account));

        if ($offer instanceof Offer) {
            $em->remove($offer);
            $em->flush();
            $this->addFlash('success', 'Предложение успешно снято.');
        } else {
            $this->addFlash('error', 'Не удалось снять предложение.');
        }

        return $this->redirect($this->generateUrl('like_sell'));
    }

    /**
     * @param Request $request
     * @param $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function historyAction(Request $request, $page)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $google_account = $this->get('google')->getCurrentGoogleAccount();

        if ($google_account !== null) {
            $order_query = $em->getRepository('AppBundle:Order')
                ->queryAllByGoogleAccount($google_account);
        } else {
            $order_query = $em->getRepository('AppBundle:Order')
                ->queryAllByUser($this->getUser());
        }

        $pagination = new Pagination($order_query, array(
            'per_page' => 10,
        ));

        return $this->render('@App/Like/history.html.twig', array(
            'pagination' => $pagination->paginate($page),
            'pagination_controls' => $pagination->getControls($request),
        ));
    }

    public function processAction(Request $request, $id)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $order = $em->getRepository('AppBundle:Order')
            ->find($id);

        $form = $this->createForm(OrderProcessType::class, $order);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if ($form->get('doit')->isClicked()) {
                if ($this->get('google')->like($order->getVideo())) {
                    if ($this->get('finance')->processOrder($order, Order::STATUS_DONE)) {
                        $this->addFlash('success', 'Заявка успешно выполнена');
                        return $this->redirect($this->generateUrl('like_index'));
                    }
                }
            } else if ($form->get('refuse')->isClicked()) {
                if ($this->get('finance')->processOrder($order, Order::STATUS_REJECTED)) {
                    $this->addFlash('success', 'Заявка успешно отколнена');
                    $message = new \Swift_Message('Ваша заявка отклонена');
                    $message
                        ->setFrom($this->get('settings')->get('mail_from'))
                        ->setTo($order->getClient()->getEmail())
                        ->setBody(
                            $this->renderView('@App/mail/order_rejected.html.twig', array(
                                'doer_name' => $order->getDoer()->getName(),
                            )),
                            'text/html'
                        );
                    $this->get('mailer')->send($message);
                    return $this->redirect($this->generateUrl('like_index'));
                }
            }
            $this->addFlash('error', 'Не удлось обработать заявку');
        }

        return $this->render('@App/Like/process.html.twig', array(
            'order' => $order,
            'form' => $form->createView(),
        ));
    }
}