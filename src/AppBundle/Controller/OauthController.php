<?php

namespace AppBundle\Controller;


use AppBundle\Entity\User;
use AppBundle\Entity\YoutubeAccount;
use AppBundle\Security\Authentication\Token\GoogleToken;
use AppBundle\Security\Authentication\Token\YoutubeToken;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class OauthController extends Controller
{
    public function authAction()
    {
        $client = $this->get('happyr.google.api.client');

        // Determine the level of access your application needs
        $client->getGoogleClient()->setScopes(array(
            \Google_Service_YouTube::YOUTUBE,
            \Google_Service_Dataflow::USERINFO_EMAIL,
        ));
        $client->getGoogleClient()->setAccessType('offline');
        $client->getGoogleClient()->setApprovalPrompt("force");

        // Send the user to complete their part of the OAuth
        return $this->redirect($client->createAuthUrl());
    }

    public function redirectAction(Request $request)
    {
        if($request->get('code'))
        {
            $code = $request->get('code');
            $client = $this->get('happyr.google.api.client');
            $client->getGoogleClient()->setScopes(array(
                \Google_Service_YouTube::YOUTUBE,
                \Google_Service_Dataflow::USERINFO_EMAIL,
            ));
            $client->authenticate($code);

            $access_token = $client->getGoogleClient()->getAccessToken();
            $result = $this->get('google')->authenticate($access_token);
            if (isset($result['new_user'])) {
                $message = new \Swift_Message('Автоматическая регистрация');
                $message
                    ->setFrom($this->get('settings')->get('mail_from'))
                    ->setTo($result['new_user']->getEmail())
                    ->setBody(
                        $this->renderView('@App/mail/auto_registration.html.twig', array(
                            'login' => $result['new_user']->getEmail(),
                            'password' => $result['new_user']->getPlainPassword(),
                        )),
                        'text/html'
                    );
                $this->get('mailer')->send($message);
            }
            if (!empty($result['message'])) {
                $this->addFlash($result['status'], $result['message']);
            }

            // Если вошел админ - перенаправляем в админку
            if ($this->getUser()->hasRole('ROLE_ADMIN')) {
                $response = $this->redirect($this->generateUrl('admin_index'));
            // Иначе в панель
            } else {
                $response = $this->redirect($this->generateUrl('like_index'));
            }
            // Обновляем запомненный токен, если авторизация была через гугл
            if (($token = $this->get('security.token_storage')->getToken()) instanceof GoogleToken) {
                $this->get('app.authentication.rememberme.services.simplehash')
                    ->loginSuccess($request, $response, $token);
            }
            return $response;
        } else {
            $error = $request->get('error');
            $this->addFlash('error', 'Ошибка авторизации: '.$error);
        }

        return $this->redirect($this->generateUrl('like_index'));
    }
}