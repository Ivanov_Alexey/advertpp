<?php

namespace AppBundle\Controller;


use AppBundle\Entity\GoogleAccount;
use AppBundle\Entity\Tip;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PanelController extends Controller
{
    public function ofertaAction()
    {
        return $this->render('@App/Panel/oferta.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function accountSwitcherAction()
    {
        $current_google_account_id = $this->get('session')->get('current_google_account');
        $google_accounts = $this->getUser()->getGoogleAccounts();
        $current = null;
        foreach ($google_accounts as $google_account) {
            if ($google_account->getId() == $current_google_account_id) {
                $current = $google_account;
            }
        }

        return $this->render('@App/Panel/_account_switcher.html.twig', array(
            'current' => $current,
            'google_accounts' => $google_accounts,
        ));
    }

    /**
     * @return Response
     */
    public function tipsAction()
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        if ($this->getUser()->getSettingShowTips()) {
            try
            {
                $tips = $em->getRepository('AppBundle:Tip')
                    ->findAll();
                return $this->render('@App/Panel/_tips.html.twig', array(
                    'tips' => $tips,
                ));
            } catch (\Exception $e) { }
        }
        
        return new Response();
    }

    /**
     * @param Request $request
     * @param $account_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function switchAccountAction(Request $request, $account_id)
    {
        $this->get('session')->set('current_google_account', $account_id);
        if (!empty($request->get('back'))) {
            return $this->redirect($request->get('back'));
        } else if (!empty($request->headers->get('referer'))) {
            return $this->redirect($request->headers->get('referer'));
        } else {
            return $this->redirect($this->generateUrl('like_index'));
        }
    }

    /**
     * @param $account_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function refreshAccountAction($account_id)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $account = $em->getRepository('AppBundle:GoogleAccount')
            ->find($account_id);
        if ($account instanceof GoogleAccount && $account->getUser()->getId() == $this->getUser()->getId()) {
            if ($this->get('google')->updateYoutubeChannel($account)) {
                $this->addFlash('success', 'Аккаунт успешно обновлен.');
                return $this->redirect($this->generateUrl('user_profile'));
            }
        }

        $this->addFlash('error', 'Не удалось обновить аккаунт. Попробуйте выполнить привязку повторно.');
        return $this->redirect($this->generateUrl('user_profile'));
    }
}