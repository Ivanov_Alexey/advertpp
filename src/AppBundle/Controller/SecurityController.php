<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Balance;
use AppBundle\Entity\User;
use AppBundle\Form\Type\RegistrationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class SecurityController extends Controller
{
    public function loginAction()
    {
        $csrfToken = $this->container->get('security.csrf.token_manager')->getToken('authenticate');

        $error = $this->get('security.authentication_utils')->getLastAuthenticationError();

        return $this->render('AppBundle:Security:login.html.twig', array(
            'csrf_token' => $csrfToken,
            'error' => $error,
        ));
    }

    public function registrationAction(Request $request)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $user = new User();

        $form = $this->createForm(RegistrationType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // Создание баланса
            $balance = new Balance();
            $balance->setUser($user);
            $balance->setAmount(0);

            // Сохранение пользователя
            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $em->persist($user);
            $em->persist($balance);

            $em->flush();

            $token = new UsernamePasswordToken($user, $user->getPassword(), 'main', $user->getRoles());
            $this->get('security.token_storage')->setToken($token);

            return $this->redirect($this->get('session')->get(
                '_security.main.target_path',
                $this->generateUrl('like_index')));
        }

        return $this->render('@App/Security/registration.html.twig', array(
            'registration_form' => $form->createView(),
        ));
    }
}