<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Tip;
use AppBundle\Form\Type\AdminTipType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TipAdminController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        
        $tips = $em->getRepository('AppBundle:Tip')
            ->findAll();

        return $this->render('AppBundle:TipAdmin:index.html.twig', array(
            'tips' => $tips,
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $tip = new Tip();
        $form = $this->createForm(AdminTipType::class, $tip);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($tip);
            $em->flush();
            $this->addFlash('success', 'Совет успешно добавлен');

            return $this->redirect($this->generateUrl('admin_tips'));
        }

        return $this->render('@App/TipAdmin/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $tip = $em->getRepository('AppBundle:Tip')
            ->find($id);
        $form = $this->createForm(AdminTipType::class, $tip);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($tip);
            $em->flush();
            $this->addFlash('success', 'Совет успешно сохранен');

            return $this->redirect($this->generateUrl('admin_tips'));
        }

        return $this->render('@App/TipAdmin/edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function deleteAction($id)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $tip = $em->getRepository('AppBundle:Tip')
            ->find($id);

        try {
            $em->remove($tip);
            $em->flush();
            $this->addFlash('success', 'Совет успешно удален');
        } catch (\Exception $e) {
            $this->addFlash('error', 'Не удалось удалить совет');
        }

        return $this->redirect($this->generateUrl('admin_tips'));
    }
}