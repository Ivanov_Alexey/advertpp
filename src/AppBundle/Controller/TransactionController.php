<?php
/**
 * Created by PhpStorm.
 * User: AlexZenter
 * Date: 23.06.2017
 * Time: 12:52
 */

namespace AppBundle\Controller;


use AppBundle\Component\Pagination;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TransactionController extends Controller
{
    /**
     * @param Request $request
     * @param $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function historyAction(Request $request, $page)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $transaction_query = $em->getRepository('AppBundle:Transaction')
            ->queryByBalance($this->getUser()->getBalance());

        $pagination = new Pagination($transaction_query, array(
            'per_page' => 10,
        ));

        return $this->render('@App/Transaction/history.html.twig', array(
            'pagination' => $pagination->paginate($page),
            'pagination_controls' => $pagination->getControls($request),
        ));
    }
}