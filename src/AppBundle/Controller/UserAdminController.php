<?php

namespace AppBundle\Controller;


use AppBundle\Component\Pagination;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserAdminController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $search = $request->get('search');
        if (!empty($search)) {
            $results = $em->getRepository('AppBundle:User')
                ->search($search);
        } else {
            $results = null;
        }

        return $this->render('AppBundle:UserAdmin:index.html.twig', array(
            'search' => $search,
            'results' => $results,
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction($id)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppBundle:User')
            ->find($id);

        return $this->render('AppBundle:UserAdmin:view.html.twig', array(
            'user' => $user,
        ));
    }

    /**
     * @param Request $request
     * @param $user_id
     * @param $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function depositsAction(Request $request, $user_id, $page)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppBundle:User')
            ->find($user_id);
        $orders_query = $em->getRepository('AppBundle:Deposit')
            ->queryAllByUser($user);

        $pagination = new Pagination($orders_query, array(
            'per_page' => 10,
        ));

        return $this->render('@App/UserAdmin/deposits.html.twig', array(
            'user' => $user,
            'pagination' => $pagination->paginate($page),
            'pagination_controls' => $pagination->getControls($request),
        ));
    }

    /**
     * @param Request $request
     * @param $user_id
     * @param $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function withdrawalsAction(Request $request, $user_id, $page)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppBundle:User')
            ->find($user_id);
        $orders_query = $em->getRepository('AppBundle:Withdraw')
            ->queryAllByUser($user);

        $pagination = new Pagination($orders_query, array(
            'per_page' => 10,
        ));

        return $this->render('@App/UserAdmin/withdrawals.html.twig', array(
            'user' => $user,
            'pagination' => $pagination->paginate($page),
            'pagination_controls' => $pagination->getControls($request),
        ));
    }

    /**
     * @param Request $request
     * @param $user_id
     * @param $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ordersAction(Request $request, $user_id, $page)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppBundle:User')
            ->find($user_id);
        $orders_query = $em->getRepository('AppBundle:Order')
            ->queryAllByUser($user);

        $pagination = new Pagination($orders_query, array(
            'per_page' => 10,
        ));

        return $this->render('@App/UserAdmin/orders.html.twig', array(
            'user' => $user,
            'pagination' => $pagination->paginate($page),
            'pagination_controls' => $pagination->getControls($request),
        ));
    }

    /**
     * @param Request $request
     * @param $user_id
     * @param $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function transactionsAction(Request $request, $user_id, $page)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppBundle:User')
            ->find($user_id);
        $transaction_query = $em->getRepository('AppBundle:Transaction')
            ->queryByBalance($user->getBalance());

        $pagination = new Pagination($transaction_query, array(
            'per_page' => 10,
        ));

        return $this->render('@App/UserAdmin/transactions.html.twig', array(
            'user' => $user,
            'pagination' => $pagination->paginate($page),
            'pagination_controls' => $pagination->getControls($request),
        ));
    }
}