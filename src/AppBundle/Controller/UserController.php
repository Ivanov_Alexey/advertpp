<?php

namespace AppBundle\Controller;


use AppBundle\Form\Type\UserChangePasswordType;
use AppBundle\Form\Type\UserProfileType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserController extends Controller
{
    public function profileAction(Request $request)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var \AppBundle\Entity\User $user */
        $user = $this->getUser();

        // Возможность сменить пароль только при входе по паролю
        if ($this->get('security.token_storage')->getToken() instanceof UsernamePasswordToken) {
            $change_password_form = $this->createForm(UserChangePasswordType::class, $user);
            $change_password_form->handleRequest($request);
            if ($change_password_form->isSubmitted() && $change_password_form->isValid()) {
                if (!empty($user->getPlainPassword())) {
                    if ($this->get('security.password_encoder')->isPasswordValid($user, $user->getOldPassword())) {
                        $password = $this->get('security.password_encoder')
                            ->encodePassword($user, $user->getPlainPassword());
                        $user->setPassword($password);
                        $em->persist($user);
                        $em->flush();
                        $this->addFlash('success', 'Пароль успешно изменен.');
                    } else {
                        $change_password_form->get('oldPassword')->addError(new FormError('Указан неверный пароль'));
                    }
                }
            }
            $change_password_form_view = $change_password_form->createView();
        } else {
            $change_password_form_view = null;
        }

        $profile_form = $this->createForm(UserProfileType::class, $user);
        $profile_form->handleRequest($request);
        if ($profile_form->isSubmitted() && $profile_form->isValid()) {
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', 'Профиль успешно обновлен.');
        }

        return $this->render('@App/User/profile.html.twig', array(
            'change_password_form' => $change_password_form_view,
            'profile_form' => $profile_form->createView(),
        ));
    }
}