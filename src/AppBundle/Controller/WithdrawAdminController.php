<?php

namespace AppBundle\Controller;


use AppBundle\Component\Pagination;
use AppBundle\Entity\Withdraw;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class WithdrawAdminController extends Controller
{
    public function indexAction(Request $request, $page)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        
        $unpaid = $em->getRepository('AppBundle:Withdraw')
            ->findBy(array('status' => Withdraw::STATUS_PROCESSING));
        
        $withdraw_query = $em->getRepository('AppBundle:Withdraw')
            ->queryFinished();

        $pagination = new Pagination($withdraw_query, array(
            'per_page' => 10,
        ));

        return $this->render('@App/WithdrawAdmin/index.html.twig', array(
            'unpaid' => $unpaid,
            'pagination' => $pagination->paginate($page),
            'pagination_controls' => $pagination->getControls($request),
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction($id)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        
        $withdraw = $em->getRepository('AppBundle:Withdraw')
            ->find($id);
        
        return $this->render('@App/WithdrawAdmin/view.html.twig', array(
            'withdraw' => $withdraw,
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processAction(Request $request, $id)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $withdraw = $em->getRepository('AppBundle:Withdraw')
            ->find($id);
        if (!empty($request->get('comment'))) {
            $withdraw->setComment($request->get('comment'));
        }
        
        if ($request->get('done') !== null) {
            if ($this->get('finance')->processWithdraw($withdraw)) {
                $this->addFlash('success', 'Заявка успешно отмечена как выполненная');
            } else {
                $this->addFlash('error', 'Не удалось выполнить заявку');
            }
        } else if ($request->get('rejected') !== null) {
            if ($this->get('finance')->cancelWithdraw($withdraw)) {
                $this->addFlash('success', 'Заявка успешно отклонена');
            } else {
                $this->addFlash('error', 'Не удалось отклонить заявку');
            }
        }

        return $this->redirect($this->generateUrl('admin_withdraw_view', array('id' => $withdraw->getId())));
    }
}