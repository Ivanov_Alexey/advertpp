<?php

namespace AppBundle\Controller;


use AppBundle\Component\Pagination;
use AppBundle\Entity\Deposit;
use AppBundle\Entity\Withdraw;
use AppBundle\Form\Type\WithdrawType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class WithdrawController extends Controller
{
    /**
     * @param Request $request
     * @param $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function historyAction(Request $request, $page)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $withdraw_query = $em->getRepository('AppBundle:Withdraw')
            ->queryAllByUser($this->getUser());

        $pagination = new Pagination($withdraw_query, array(
            'per_page' => 10,
        ));

        return $this->render('@App/Withdraw/history.html.twig', array(
            'pagination' => $pagination->paginate($page),
            'pagination_controls' => $pagination->getControls($request),
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $withdraw = new Withdraw();
        $withdraw
            ->setUser($this->getUser())
            ->setType(Deposit::TYPE_YANDEX)
            ->setAccount($this->getUser()->getYandexMoneyAccount());
        $form = $this->createForm(WithdrawType::class, $withdraw);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->get('finance')->createWithdraw($withdraw)) {
                $this->addFlash('success', 'Запрос на вывод средств успешно принят');
                return $this->redirect($this->generateUrl('withdraw_history'));
            } else {
                $this->addFlash('error', 'Не удалось разместить запрос на вывод средств');
            }
        }

        return $this->render('@App/Withdraw/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function cancelAction($id)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $withdraw = $em->getRepository('AppBundle:Withdraw')->find($id);

        if (!($withdraw instanceof Withdraw) || 
            $withdraw->getUser()->getId() != $this->getUser()->getId()) {
            throw $this->createNotFoundException();
        }

        if ($withdraw->getStatus() == Withdraw::STATUS_PROCESSING) {
            $this->get('finance')->cancelWithdraw($withdraw, Withdraw::STATUS_CANCELED);
            $this->addFlash('success', 'Заявка на вывод средств успешно отменена.');
        } else {
            $this->addFlash('error', 'Не удалось отменить заявку на вывод средств: заявка уже обработана.');
        }
        return $this->redirect($this->generateUrl('withdraw_history'));
    }
}