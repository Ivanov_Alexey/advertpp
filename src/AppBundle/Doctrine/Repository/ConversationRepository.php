<?php

namespace AppBundle\Doctrine\Repository;


use Doctrine\ORM\EntityRepository;

class ConversationRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\Query
     */
    public function queryAllOrderByStatusDate()
    {
        return $this->_em->createQueryBuilder()
            ->select('conversation')
            ->from('AppBundle:Conversation', 'conversation')
            ->orderBy('conversation.updated_at', 'DESC')
            ->orderBy('conversation.status', 'DESC')
            ->getQuery();
    }

    /**
     * @param \AppBundle\Entity\User $user
     * @return \Doctrine\ORM\Query
     */
    public function queryAllByUserOrderByUpdated($user)
    {
        return $this->_em->createQueryBuilder()
            ->select('conversation')
            ->from('AppBundle:Conversation', 'conversation')
            ->where('conversation.starter = :user')
            ->setParameter('user', $user)
            ->orderBy('conversation.updated_at', 'DESC')
            ->getQuery();
    }
}