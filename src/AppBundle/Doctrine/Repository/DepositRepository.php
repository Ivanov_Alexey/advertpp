<?php

namespace AppBundle\Doctrine\Repository;


use AppBundle\Entity\Deposit;
use Doctrine\ORM\EntityRepository;

class DepositRepository extends EntityRepository
{
    public function findExpired()
    {
        return $this->_em->createQueryBuilder()
            ->select(array('deposit'))
            ->from('AppBundle:Deposit', 'deposit')
            ->where('(deposit.status = :new OR deposit.status = :processing) AND deposit.created_at < :date')
            ->setParameters(array(
                'new' => Deposit::STATUS_NEW,
                'processing' => Deposit::STATUS_PROCESSING,
                'date' => new \DateTime('-1 hour')
            ))
            ->getQuery()
            ->getResult();
    }

    public function getQueryAll()
    {
        return $this->_em->createQueryBuilder()
            ->select(array('deposit', 'user'))
            ->from('AppBundle:Deposit', 'deposit')
            ->join('deposit.user', 'user')
            ->orderBy('deposit.created_at', 'DESC')
            ->getQuery();
    }

    /**
     * @param \AppBundle\Entity\User $user
     * @return \Doctrine\ORM\Query
     */
    public function queryAllByUser($user)
    {
        return $this->_em->createQueryBuilder()
            ->select(array('deposit'))
            ->from('AppBundle:Deposit', 'deposit')
            ->where('deposit.user = :user')
            ->setParameter('user', $user)
            ->orderBy('deposit.created_at', 'DESC')
            ->getQuery();
    }
}