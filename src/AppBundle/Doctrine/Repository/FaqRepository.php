<?php

namespace AppBundle\Doctrine\Repository;


use Doctrine\ORM\EntityRepository;

class FaqRepository extends EntityRepository
{
    public function findAllOrdered()
    {
        return $this->_em->createQueryBuilder()
            ->select(array('faq'))
            ->from('AppBundle:Faq', 'faq')
            ->orderBy('faq.position', 'ASC')
            ->getQuery()
            ->getResult();
    }
}