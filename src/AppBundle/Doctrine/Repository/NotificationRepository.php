<?php

namespace AppBundle\Doctrine\Repository;


use AppBundle\Entity\Notification;
use AppBundle\Entity\User;
use AppBundle\Service\Notification\NotifiableInterface;
use Doctrine\ORM\EntityRepository;

class NotificationRepository extends EntityRepository
{
    /**
     * @param User $user
     * @param NotifiableInterface $notifiable
     * @return Notification[]
     */
    public function findByUserAndSourceID($user, $notifiable)
    {
        return $this->_em->createQueryBuilder()
            ->select('notification')
            ->from('AppBundle:Notification', 'notification')
            ->where('notification.user = :user')
            ->andWhere('notification.source = :source')
            ->andWhere('notification.source_id = :source_id')
            ->setParameters(array(
                'user' => $user,
                'source' => get_class($notifiable),
                'source_id' => $notifiable->getId(),
            ))
            ->orderBy('notification.created_at', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param User $user
     * @param string $source
     * @return int
     */
    public function countByUserAndSource($user, $source)
    {
        return $this->_em->createQueryBuilder()
            ->select('count(notification)')
            ->from('AppBundle:Notification', 'notification')
            ->where('notification.user = :user')
            ->andWhere('notification.source = :source')
            ->setParameters(array(
                'user' => $user,
                'source' => $source,
            ))
            ->getQuery()
            ->getSingleScalarResult();
    }
}