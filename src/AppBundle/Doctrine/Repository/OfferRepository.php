<?php

namespace AppBundle\Doctrine\Repository;


use Doctrine\ORM\EntityRepository;

class OfferRepository extends EntityRepository
{
    /**
     * @param \AppBundle\Entity\User $user
     * @return \AppBundle\Entity\Offer[]
     */
    public function findAvailableForUser($user)
    {
        return $this->_em->createQueryBuilder()
            ->select(array('offer', 'google_account'))
            ->from('AppBundle:Offer', 'offer')
            ->join('offer.google_account', 'google_account')
            ->where('google_account.user != :user')
            //->andWhere('offer.full_price <= :balance')
            ->setParameters(array(
                'user' => $user,
                //'balance' => $user->getBalance()->getAmount()
            ))
            ->orderBy('offer.full_price', 'ASC')
            ->getQuery()
            ->getResult();
    }
}