<?php

namespace AppBundle\Doctrine\Repository;

use AppBundle\Entity\Order;
use Doctrine\ORM\EntityRepository;

class OrderRepository extends EntityRepository
{
    /**
     * @param \AppBundle\Entity\GoogleAccount $google_account
     * @return Order[]
     */
    public function findWaitingByDoerGoogleAccount($google_account)
    {
        return $this->_em->createQueryBuilder()
            ->select(array('o', 'client', 'client_google_account'))
            ->from('AppBundle:Order', 'o')
            ->join('o.client', 'client')
            ->leftJoin('o.client_google_account', 'client_google_account')
            ->where('o.doer_google_account = :doer_google_account')
            ->andWhere('o.status = :status')
            ->setParameters(array(
                'doer_google_account' => $google_account,
                'status' => Order::STATUS_WAITING,
            ))
            ->orderBy('o.created_at', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param \AppBundle\Entity\GoogleAccount $google_account
     * @return Order[]
     */
    public function findWaitingByClientGoogleAccount($google_account)
    {
        return $this->_em->createQueryBuilder()
            ->select(array('o', 'doer_google_account'))
            ->from('AppBundle:Order', 'o')
            ->join('o.doer_google_account', 'doer_google_account')
            ->where('o.client_google_account = :client_google_account')
            ->andWhere('o.status = :status')
            ->setParameters(array(
                'client_google_account' => $google_account,
                'status' => Order::STATUS_WAITING,
            ))
            ->orderBy('o.created_at')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param \AppBundle\Entity\User $user
     * @return Order[]
     */
    public function findWaitingByClient($user)
    {
        return $this->_em->createQueryBuilder()
            ->select(array('o', 'doer_google_account'))
            ->from('AppBundle:Order', 'o')
            ->join('o.doer_google_account', 'doer_google_account')
            ->where('o.client = :client')
            ->andWhere('o.status = :status')
            ->setParameters(array(
                'client' => $user,
                'status' => Order::STATUS_WAITING,
            ))
            ->orderBy('o.created_at')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param \AppBundle\Entity\GoogleAccount $google_account
     * @param int $limit
     * @return Order[]
     */
    public function findFailedByGoogleAccount($google_account, $limit = 6)
    {
        $qb = $this->_em->createQueryBuilder();
        return $qb->select(array('o', 'doer_google_account', 'client_google_account'))
            ->from('AppBundle:Order', 'o')
            ->join('o.doer_google_account', 'doer_google_account')
            ->leftJoin('o.client_google_account', 'client_google_account')
            ->join('o.client', 'client')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->in('o.status', array(Order::STATUS_EXPIRED, Order::STATUS_REJECTED)),
                    $qb->expr()->orX(
                        $qb->expr()->eq('o.client_google_account', ':google_account'),
                        $qb->expr()->eq('o.doer_google_account', ':google_account')
                    )
                )
            )
            ->setParameter(':google_account', $google_account)
            ->orderBy('o.closed_at', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param \AppBundle\Entity\User $user
     * @param int $limit
     * @return Order[]
     */
    public function findFailedByUser($user, $limit = 6)
    {
        $qb = $this->_em->createQueryBuilder();
        return $qb->select(array('o', 'doer_google_account', 'client_google_account'))
            ->from('AppBundle:Order', 'o')
            ->join('o.doer_google_account', 'doer_google_account')
            ->leftJoin('o.client_google_account', 'client_google_account')
            ->join('o.client', 'client')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->in('o.status', array(Order::STATUS_EXPIRED, Order::STATUS_REJECTED)),
                    $qb->expr()->orX(
                        $qb->expr()->eq('o.client', ':user'),
                        $qb->expr()->eq('o.doer', ':user')
                    )
                )
            )
            ->setParameter('user', $user)
            ->orderBy('o.closed_at', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param \AppBundle\Entity\GoogleAccount $google_account
     * @param int $limit
     * @return Order[]
     */
    public function findSucceedByGoogleAccount($google_account, $limit = 6)
    {
        $qb = $this->_em->createQueryBuilder();
        return $qb->select(array('o', 'doer_google_account', 'client_google_account'))
            ->from('AppBundle:Order', 'o')
            ->join('o.doer_google_account', 'doer_google_account')
            ->leftJoin('o.client_google_account', 'client_google_account')
            ->join('o.client', 'client')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('o.status', ':status'),
                    $qb->expr()->orX(
                        $qb->expr()->eq('o.client_google_account', ':google_account'),
                        $qb->expr()->eq('o.doer_google_account', ':google_account')
                    )
                )
            )
            ->setParameters(array(
                'google_account' => $google_account,
                'status' => Order::STATUS_DONE,
            ))
            ->orderBy('o.closed_at', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param \AppBundle\Entity\User $user
     * @param int $limit
     * @return Order[]
     */
    public function findSucceedByUser($user, $limit = 6)
    {
        $qb = $this->_em->createQueryBuilder();
        return $qb->select(array('o', 'doer_google_account', 'client_google_account'))
            ->from('AppBundle:Order', 'o')
            ->join('o.doer_google_account', 'doer_google_account')
            ->leftJoin('o.client_google_account', 'client_google_account')
            ->join('o.client', 'client')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('o.status', ':status'),
                    $qb->expr()->orX(
                        $qb->expr()->eq('o.client', ':user'),
                        $qb->expr()->eq('o.doer', ':user')
                    )
                )
            )
            ->setParameters(array(
                'user' => $user,
                'status' => Order::STATUS_DONE,
            ))
            ->orderBy('o.closed_at', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param \AppBundle\Entity\GoogleAccount $google_account
     * @return \Doctrine\ORM\Query
     */
    public function queryAllByGoogleAccount($google_account)
    {
        $qb = $this->_em->createQueryBuilder();
        return $qb->select(array('o', 'doer_google_account', 'client_google_account'))
            ->from('AppBundle:Order', 'o')
            ->join('o.doer_google_account', 'doer_google_account')
            ->leftJoin('o.client_google_account', 'client_google_account')
            ->join('o.client', 'client')
            ->where(
                $qb->expr()->orX(
                    $qb->expr()->eq('o.client_google_account', ':google_account'),
                    $qb->expr()->eq('o.doer_google_account', ':google_account')
                )
            )
            ->setParameter('google_account', $google_account)
            ->orderBy('o.created_at', 'DESC')
            ->getQuery();
    }

    /**
     * @param \AppBundle\Entity\User $user
     * @return \Doctrine\ORM\Query
     */
    public function queryAllByUser($user)
    {
        $qb = $this->_em->createQueryBuilder();
        return $qb->select(array('o', 'doer_google_account', 'client_google_account'))
            ->from('AppBundle:Order', 'o')
            ->join('o.doer_google_account', 'doer_google_account')
            ->leftJoin('o.client_google_account', 'client_google_account')
            ->join('o.client', 'client')
            ->where(
                $qb->expr()->orX(
                    $qb->expr()->eq('o.client', ':user'),
                    $qb->expr()->eq('o.doer', ':user')
                )
            )
            ->setParameter('user', $user)
            ->orderBy('o.created_at', 'DESC')
            ->getQuery();
    }

    /**
     * @return Order[]
     */
    public function findExpired()
    {
        $qb = $this->_em->createQueryBuilder();
        return $qb->select('o')
            ->from('AppBundle:Order', 'o')
            ->where('o.created_at < :date')
            ->andWhere('o.status = :status')
            ->setParameters(array(
                'date' => new \DateTime('-1 day'),
                'status' => Order::STATUS_WAITING,
            ))
            ->getQuery()
            ->getResult();
    }
}