<?php

namespace AppBundle\Doctrine\Repository;


use Doctrine\ORM\EntityRepository;

class TipRepository extends EntityRepository
{
    /**
     * @return \AppBundle\Entity\Tip
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findRandom()
    {
        return $this->_em->createQueryBuilder()
            ->select('tip, RAND() as HIDDEN rand')
            ->from('AppBundle:Tip', 'tip')
            ->orderBy('rand')
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult();
    }
}