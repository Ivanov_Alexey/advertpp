<?php

namespace AppBundle\Doctrine\Repository;


use Doctrine\ORM\EntityRepository;

class TransactionRepository extends EntityRepository
{
    /**
     * @param \AppBundle\Entity\Balance $balance
     * @return \Doctrine\ORM\Query
     */
    public function queryByBalance($balance)
    {
        return $this->_em->createQueryBuilder()
            ->select(array('transaction'))
            ->from('AppBundle:Transaction', 'transaction')
            ->where('transaction.balance = :balance')
            ->setParameter('balance', $balance)
            ->orderBy('transaction.created_at', 'DESC')
            ->getQuery();
    }
}