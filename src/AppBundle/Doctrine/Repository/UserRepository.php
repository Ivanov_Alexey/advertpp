<?php

namespace AppBundle\Doctrine\Repository;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    /**
     * @param string $search
     * @return User[]
     */
    public function search($search)
    {
        return $this->_em->createQueryBuilder()
            ->select(array('user','google_account'))
            ->from('AppBundle:User', 'user')
            ->leftJoin('user.google_accounts', 'google_account')
            ->where('user.name LIKE :search')
            ->orWhere('user.email LIKE :search')
            ->orWhere('google_account.channel_name LIKE :search')
            ->setParameter('search', '%'.$search.'%')
            ->getQuery()
            ->getResult();
    }
}