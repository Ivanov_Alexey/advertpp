<?php

namespace AppBundle\Doctrine\Repository;


use AppBundle\Entity\Withdraw;
use Doctrine\ORM\EntityRepository;

class WithdrawRepository extends EntityRepository
{
    /**
     * @param \AppBundle\Entity\User $user
     * @return \Doctrine\ORM\Query
     */
    public function queryAllByUser($user)
    {
        return $this->_em->createQueryBuilder()
            ->select(array('withdraw'))
            ->from('AppBundle:Withdraw', 'withdraw')
            ->where('withdraw.user = :user')
            ->setParameter('user', $user)
            ->orderBy('withdraw.created_at', 'DESC')
            ->getQuery();
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function queryFinished()
    {
        return $this->_em->createQueryBuilder()
            ->select(array('withdraw', 'user'))
            ->from('AppBundle:Withdraw', 'withdraw')
            ->join('withdraw.user', 'user')
            ->where('withdraw.status <> :status')
            ->setParameter('status', Withdraw::STATUS_PROCESSING)
            ->orderBy('withdraw.created_at', 'DESC')
            ->getQuery();
    }
}