<?php
namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Doctrine\Repository\ConversationRepository")
 * @ORM\Table(name="`conversation`")
 */
class Conversation
{
    const STATUS_CLOSED = 1;
    const STATUS_OPENED = 2;
    const STATUS_REOPENED = 3;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="starter_id", referencedColumnName="id")
     */
    protected $starter;

    /**
     * @Assert\NotBlank(message="Укажите тему обращения")
     * @ORM\Column(type="string", length=255)
     */
    protected $subject;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $status = self::STATUS_OPENED;

    /**
     * @ORM\OneToMany(targetEntity="Message", mappedBy="conversation")
     */
    protected $messages;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated_at;

    /**
     * @Assert\Valid()
     * @var \AppBundle\Entity\Message
     */
    protected $start_message;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return Conversation
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Conversation
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Conversation
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set starter
     *
     * @param \AppBundle\Entity\User $starter
     *
     * @return Conversation
     */
    public function setStarter(\AppBundle\Entity\User $starter = null)
    {
        $this->starter = $starter;

        return $this;
    }

    /**
     * Get starter
     *
     * @return \AppBundle\Entity\User
     */
    public function getStarter()
    {
        return $this->starter;
    }

    /**
     * Add message
     *
     * @param \AppBundle\Entity\Message $message
     *
     * @return Conversation
     */
    public function addMessage(\AppBundle\Entity\Message $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove message
     *
     * @param \AppBundle\Entity\Message $message
     */
    public function removeMessage(\AppBundle\Entity\Message $message)
    {
        $this->messages->removeElement($message);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @return \AppBundle\Entity\Message
     */
    public function getStartMessage()
    {
        return $this->start_message;
    }

    /**
     * @param \AppBundle\Entity\Message $start_message
     */
    public function setStartMessage($start_message)
    {
        $this->start_message = $start_message;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Conversation
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }
}
