<?php

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="`google_account`")
 */
class GoogleAccount
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $google_id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="google_accounts")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\Column(type="array")
     */
    protected $token;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $channel_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $channel_name;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    protected $channel_images;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $uploads_playlist_id;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    protected $channel_stats;

    public function getShortChannelName()
    {
        if (strlen($this->channel_name) > 15) {
            return substr($this->channel_name, 0, 15).'...';
        } else {
            return $this->channel_name;
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set googleId
     *
     * @param string $googleId
     *
     * @return GoogleAccount
     */
    public function setGoogleId($googleId)
    {
        $this->google_id = $googleId;

        return $this;
    }

    /**
     * Get googleId
     *
     * @return string
     */
    public function getGoogleId()
    {
        return $this->google_id;
    }

    /**
     * Set token
     *
     * @param array $token
     *
     * @return GoogleAccount
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return array
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return GoogleAccount
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set channelId
     *
     * @param string $channelId
     *
     * @return GoogleAccount
     */
    public function setChannelId($channelId)
    {
        $this->channel_id = $channelId;

        return $this;
    }

    /**
     * Get channelId
     *
     * @return string
     */
    public function getChannelId()
    {
        return $this->channel_id;
    }

    /**
     * Set channelName
     *
     * @param string $channelName
     *
     * @return GoogleAccount
     */
    public function setChannelName($channelName)
    {
        $this->channel_name = $channelName;

        return $this;
    }

    /**
     * Get channelName
     *
     * @return string
     */
    public function getChannelName()
    {
        return $this->channel_name;
    }

    /**
     * Set channelImages
     *
     * @param array $channelImages
     *
     * @return GoogleAccount
     */
    public function setChannelImages($channelImages)
    {
        $this->channel_images = $channelImages;

        return $this;
    }

    /**
     * Get channelImages
     *
     * @return array
     */
    public function getChannelImages()
    {
        return $this->channel_images;
    }

    /**
     * Set uploadsPlaylistId
     *
     * @param string $uploadsPlaylistId
     *
     * @return GoogleAccount
     */
    public function setUploadsPlaylistId($uploadsPlaylistId)
    {
        $this->uploads_playlist_id = $uploadsPlaylistId;

        return $this;
    }

    /**
     * Get uploadsPlaylistId
     *
     * @return string
     */
    public function getUploadsPlaylistId()
    {
        return $this->uploads_playlist_id;
    }

    /**
     * Set channelStats
     *
     * @param array $channelStats
     *
     * @return GoogleAccount
     */
    public function setChannelStats($channelStats)
    {
        $this->channel_stats = $channelStats;

        return $this;
    }

    /**
     * Get channelStats
     *
     * @return array
     */
    public function getChannelStats()
    {
        return $this->channel_stats;
    }
}
