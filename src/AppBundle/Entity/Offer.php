<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Doctrine\Repository\OfferRepository")
 * @ORM\Table(name="`offer`")
 */
class Offer
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="GoogleAccount")
     * @ORM\JoinColumn(name="google_account_id", referencedColumnName="id")
     */
    protected $google_account;

    /**
     * @Assert\NotBlank()
     * @Assert\Range(min=1)
     * @ORM\Column(type="float")
     */
    protected $price;

    /**
     * @ORM\Column(type="float")
     */
    protected $full_price;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $comment;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Offer
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Offer
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set googleAccount
     *
     * @param \AppBundle\Entity\GoogleAccount $googleAccount
     *
     * @return Offer
     */
    public function setGoogleAccount(\AppBundle\Entity\GoogleAccount $googleAccount = null)
    {
        $this->google_account = $googleAccount;

        return $this;
    }

    /**
     * Get googleAccount
     *
     * @return \AppBundle\Entity\GoogleAccount
     */
    public function getGoogleAccount()
    {
        return $this->google_account;
    }

    /**
     * Set fullPrice
     *
     * @param float $fullPrice
     *
     * @return Offer
     */
    public function setFullPrice($fullPrice)
    {
        $this->full_price = $fullPrice;

        return $this;
    }

    /**
     * Get fullPrice
     *
     * @return float
     */
    public function getFullPrice()
    {
        return $this->full_price;
    }
}
