<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Doctrine\Repository\OrderRepository")
 * @ORM\Table(name="`order`")
 */
class Order
{
    const STATUS_WAITING = 'waiting';
    const STATUS_REJECTED = 'rejected';
    const STATUS_EXPIRED = 'expired';
    const STATUS_DONE = 'done';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    protected $client;

    /**
     * @ORM\ManyToOne(targetEntity="GoogleAccount")
     * @ORM\JoinColumn(name="client_google_account_id", referencedColumnName="id", nullable=true)
     */
    protected $client_google_account;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="doer_id", referencedColumnName="id")
     */
    protected $doer;

    /**
     * @ORM\ManyToOne(targetEntity="GoogleAccount")
     * @ORM\JoinColumn(name="doer_google_account_id", referencedColumnName="id")
     */
    protected $doer_google_account;

    /**
     * @ORM\Column(type="string", length=32)
     */
    protected $video;

    /**
     * @Assert\NotBlank(message="Укажите ссылку на видео", groups={"create"})
     * @Assert\Regex("~(?:http(?:s?):\/\/)?(?:www\.)?youtu\.?be(?:\.com)?\/?.*(?:watch|embed)?(?:.*v=|v\/|\/)([\w\-_]{11})\&?~", message="Не удалось идентифицировать видео. Проверьте правильность ссылки.")
     */
    protected $video_link;

    /**
     * @ORM\Column(type="float")
     */
    protected $price;

    /**
     * @ORM\Column(type="float")
     */
    protected $full_price;

    /**
     * @ORM\Column(type="string", length=32)
     */
    protected $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $doer_comment;

    /**
     * @Assert\NotBlank(groups={"create"}, message="Выберите исполнителя")
     */
    protected $offer_id;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $closed_at;

    /**
     * Extract video id from link
     * @return bool
     */
    public function processVideoLink()
    {
        if (preg_match('~(?:http(?:s?):\/\/)?(?:www\.)?youtu\.?be(?:\.com)?\/?.*(?:watch|embed)?(?:.*v=|v\/|\/)([\w\-_]{11})\&?~', $this->video_link, $matches)) {
            $this->video = $matches[1];
            return true;
        }
        return false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set video
     *
     * @param string $video
     *
     * @return Order
     */
    public function setVideo($video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return string
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Order
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getOfferId()
    {
        return $this->offer_id;
    }

    /**
     * @param mixed $offer_id
     */
    public function setOfferId($offer_id)
    {
        $this->offer_id = $offer_id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Order
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set closedAt
     *
     * @param \DateTime $closedAt
     *
     * @return Order
     */
    public function setClosedAt($closedAt)
    {
        $this->closed_at = $closedAt;

        return $this;
    }

    /**
     * Get closedAt
     *
     * @return \DateTime
     */
    public function getClosedAt()
    {
        return $this->closed_at;
    }

    /**
     * Set doerComment
     *
     * @param string $doerComment
     *
     * @return Order
     */
    public function setDoerComment($doerComment)
    {
        $this->doer_comment = $doerComment;

        return $this;
    }

    /**
     * Get doerComment
     *
     * @return string
     */
    public function getDoerComment()
    {
        return $this->doer_comment;
    }

    /**
     * Set doerGoogleAccount
     *
     * @param \AppBundle\Entity\GoogleAccount $doerGoogleAccount
     *
     * @return Order
     */
    public function setDoerGoogleAccount(\AppBundle\Entity\GoogleAccount $doerGoogleAccount = null)
    {
        $this->doer_google_account = $doerGoogleAccount;

        return $this;
    }

    /**
     * Get doerGoogleAccount
     *
     * @return \AppBundle\Entity\GoogleAccount
     */
    public function getDoerGoogleAccount()
    {
        return $this->doer_google_account;
    }

    /**
     * Set clientGoogleAccount
     *
     * @param \AppBundle\Entity\GoogleAccount $clientGoogleAccount
     *
     * @return Order
     */
    public function setClientGoogleAccount(\AppBundle\Entity\GoogleAccount $clientGoogleAccount = null)
    {
        $this->client_google_account = $clientGoogleAccount;

        return $this;
    }

    /**
     * Get clientGoogleAccount
     *
     * @return \AppBundle\Entity\GoogleAccount
     */
    public function getClientGoogleAccount()
    {
        return $this->client_google_account;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\User $client
     *
     * @return Order
     */
    public function setClient(\AppBundle\Entity\User $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\User
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set doer
     *
     * @param \AppBundle\Entity\User $doer
     *
     * @return Order
     */
    public function setDoer(\AppBundle\Entity\User $doer = null)
    {
        $this->doer = $doer;

        return $this;
    }

    /**
     * Get doer
     *
     * @return \AppBundle\Entity\User
     */
    public function getDoer()
    {
        return $this->doer;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Order
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set fullPrice
     *
     * @param float $fullPrice
     *
     * @return Order
     */
    public function setFullPrice($fullPrice)
    {
        $this->full_price = $fullPrice;

        return $this;
    }

    /**
     * Get fullPrice
     *
     * @return float
     */
    public function getFullPrice()
    {
        return $this->full_price;
    }

    /**
     * @return mixed
     */
    public function getVideoLink()
    {
        return $this->video_link;
    }

    /**
     * @param mixed $video_link
     */
    public function setVideoLink($video_link)
    {
        $this->video_link = $video_link;
    }
}
