<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Doctrine\Repository\UserRepository")
 * @ORM\Table(name="`user`")
 */
class User implements UserInterface, \Serializable
{
    const SETTING_DASHBOARD_DISPLAY_TABLE = 1;
    const SETTING_DASHBOARD_DISPLAY_TILES = 2;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     * @ORM\Column(type="string", length=255)
     */
    protected $email;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $password;

    /**
     * @Assert\NotBlank(groups={"change_password"})
     */
    protected $oldPassword;

    /**
     * @Assert\NotBlank(groups={"registration", "change_password"})
     */
    protected $plainPassword;

    /**
     * @ORM\Column(type="array")
     */
    protected $roles = array();

    /**
     * @ORM\OneToOne(targetEntity="Balance", mappedBy="user")
     */
    protected $balance;

    /**
     * @ORM\OneToMany(targetEntity="GoogleAccount", mappedBy="user")
     */
    protected $google_accounts;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $yandex_money_account;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $webmoney_account;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $qiwi_account;

    /**
     * @ORM\Column(type="smallint")
     */
    protected $setting_dashboard_display = 1;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $setting_show_tips = true;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        if (in_array('ROLE_USER', $this->roles)) {
            return $this->roles;
        } else {
            return array_merge(
                array('ROLE_USER'),
                $this->roles
            );
        }
    }

    public function hasRole($role)
    {
        return in_array($role, $this->roles);
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set roles
     *
     * @param array $roles
     *
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return mixed
     */
    public function getOldPassword()
    {
        return $this->oldPassword;
    }

    /**
     * @param mixed $oldPassword
     */
    public function setOldPassword($oldPassword)
    {
        $this->oldPassword = $oldPassword;
    }

    /**
     * Serializes the user.
     *
     * The serialized data have to contain the fields used by the equals method and the username.
     *
     * @return string
     */
    public function serialize()
    {
        return serialize(array(
            $this->password,
            $this->name,
            $this->email,
            $this->id,
        ));
    }

    /**
     * Unserializes the user.
     *
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        $data = unserialize($serialized);
        list(
            $this->password,
            $this->name,
            $this->email,
            $this->id,
            ) = $data;
    }

    /**
     * Set balance
     *
     * @param \AppBundle\Entity\Balance $balance
     *
     * @return User
     */
    public function setBalance(\AppBundle\Entity\Balance $balance = null)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance
     *
     * @return \AppBundle\Entity\Balance
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set yandexMoneyAccount
     *
     * @param string $yandexMoneyAccount
     *
     * @return User
     */
    public function setYandexMoneyAccount($yandexMoneyAccount)
    {
        $this->yandex_money_account = $yandexMoneyAccount;

        return $this;
    }

    /**
     * Get yandexMoneyAccount
     *
     * @return string
     */
    public function getYandexMoneyAccount()
    {
        return $this->yandex_money_account;
    }

    /**
     * Set webmoneyAccount
     *
     * @param string $webmoneyAccount
     *
     * @return User
     */
    public function setWebmoneyAccount($webmoneyAccount)
    {
        $this->webmoney_account = $webmoneyAccount;

        return $this;
    }

    /**
     * Get webmoneyAccount
     *
     * @return string
     */
    public function getWebmoneyAccount()
    {
        return $this->webmoney_account;
    }

    /**
     * Set qiwiAccount
     *
     * @param string $qiwiAccount
     *
     * @return User
     */
    public function setQiwiAccount($qiwiAccount)
    {
        $this->qiwi_account = $qiwiAccount;

        return $this;
    }

    /**
     * Get qiwiAccount
     *
     * @return string
     */
    public function getQiwiAccount()
    {
        return $this->qiwi_account;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->google_accounts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add googleAccount
     *
     * @param \AppBundle\Entity\GoogleAccount $googleAccount
     *
     * @return User
     */
    public function addGoogleAccount(\AppBundle\Entity\GoogleAccount $googleAccount)
    {
        $this->google_accounts[] = $googleAccount;

        return $this;
    }

    /**
     * Remove googleAccount
     *
     * @param \AppBundle\Entity\GoogleAccount $googleAccount
     */
    public function removeGoogleAccount(\AppBundle\Entity\GoogleAccount $googleAccount)
    {
        $this->google_accounts->removeElement($googleAccount);
    }

    /**
     * Get googleAccounts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGoogleAccounts()
    {
        return $this->google_accounts;
    }

    /**
     * Set settingDashboardDisplay
     *
     * @param integer $settingDashboardDisplay
     *
     * @return User
     */
    public function setSettingDashboardDisplay($settingDashboardDisplay)
    {
        $this->setting_dashboard_display = $settingDashboardDisplay;

        return $this;
    }

    /**
     * Get settingDashboardDisplay
     *
     * @return integer
     */
    public function getSettingDashboardDisplay()
    {
        return $this->setting_dashboard_display;
    }

    /**
     * Set settingShowTips
     *
     * @param boolean $settingShowTips
     *
     * @return User
     */
    public function setSettingShowTips($settingShowTips)
    {
        $this->setting_show_tips = $settingShowTips;

        return $this;
    }

    /**
     * Get settingShowTips
     *
     * @return boolean
     */
    public function getSettingShowTips()
    {
        return $this->setting_show_tips;
    }
}
