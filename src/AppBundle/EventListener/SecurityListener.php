<?php

namespace AppBundle\EventListener;


use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class SecurityListener
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * @var TokenStorage
     */
    protected $token_storage;
    /**
     * SecurityListener constructor.
     * @param TokenStorage $token_storage
     * @param Session $session
     */
    public function __construct(TokenStorage $token_storage, Session $session)
    {
        $this->session = $session;
        $this->token_storage = $token_storage;
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        if (!$this->session->has('current_youtube_account')) {
            /** @var \Doctrine\Common\Collections\ArrayCollection $youtube_accounts */
            $google_accounts = $this->token_storage->getToken()->getUser()->getGoogleAccounts();
            if ($google_accounts->count()) {
                $this->session->set('current_google_account', $google_accounts->first()->getId());
            } else {
                $this->session->set('current_google_account', 0);
            }
        }
    }
}