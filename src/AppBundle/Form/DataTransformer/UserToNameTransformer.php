<?php

namespace AppBundle\Form\DataTransformer;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class UserToNameTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManager
     */
    private $manager;

    public function __construct(EntityManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Transforms a value from the original representation to a transformed representation.
     *
     * @param \AppBundle\Entity\User $user The value in the original representation
     * @return mixed The value in the transformed representation
     * @throws TransformationFailedException When the transformation fails.
     */
    public function transform($user)
    {
        if (null === $user) {
            return '';
        }

        return $user->getUsername();
    }

    /**
     * Transforms a value from the transformed representation to its original
     * representation.
     *
     * @param mixed $name The value in the transformed representation
     * @return mixed The value in the original representation
     * @throws TransformationFailedException When the transformation fails.
     */
    public function reverseTransform($name)
    {
        if (!$name) {
            return null;
        }

        $user = $this->manager->getRepository('AppBundle:User')
            ->findOneBy(array('name' => $name));

        if (null === $user) {
            throw new TransformationFailedException(sprintf(
                'Пользаватель с именем "%s" не существует!',
                $name
            ));
        }

        return $user;
    }
}