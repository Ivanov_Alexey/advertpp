<?php

namespace AppBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminFaqType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('question', TextareaType::class, array(
                'label' => 'Текст вопроса',
            ))
            ->add('answer', TextareaType::class, array(
                'label' => 'Текст ответа',
            ))
            ->add('position', NumberType::class, array(
                'label' => 'Порядок сортировки',
            ))
            ->add('submit', SubmitType::class, array(
                'label' => $options['new'] ? 'Добавить запись' : 'Сохранить'
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Faq',
            'attr' => array('novalidate' => 'novalidate'),
            'new' => true,
        ));
    }
}