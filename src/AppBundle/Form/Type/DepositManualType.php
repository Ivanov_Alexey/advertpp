<?php

namespace AppBundle\Form\Type;

use AppBundle\Form\DataTransformer\UserToNameTransformer;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DepositManualType extends AbstractType
{
    /**
     * @var EntityManager
     */
    private $manager;

    public function __construct(EntityManager $manager)
    {
        $this->manager = $manager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('amount', NumberType::class, array(
                'label' => 'Сумма',
            ))
            ->add('user', TextType::class, array(
                'label' => 'Имя пользователя',
                'invalid_message' => 'Пользователь не найден. Укажите полное имя пользователя в системе.',
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Подвердить пополнение',
            ))
        ;

        $builder->get('user')
            ->addModelTransformer(new UserToNameTransformer($this->manager));
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Deposit',
            'attr' => array('novalidate' => 'novalidate'),
        ));
    }
}