<?php

namespace AppBundle\Form\Type;


use AppBundle\Entity\Deposit;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DepositType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('amount', NumberType::class, array(
                'label' => 'Сумма'
            ))
            ->add('type', ChoiceType::class, array(
                'choices' => array(
                    'Yandex.Money' => Deposit::TYPE_YANDEX,
                    'WebMoney' => Deposit::TYPE_WEBMONEY,
                    //'QIWI' => Deposit::TYPE_QIWI,
                ),
                'label' => 'Способ оплаты',
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Пополнить',
            ))
        ;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Deposit',
            'attr' => array('novalidate' => 'novalidate'),
        ));
    }
}