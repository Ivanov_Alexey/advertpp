<?php

namespace AppBundle\Form\Type;


use AppBundle\Entity\Setting;
use AppBundle\Service\Settings;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SettingsType extends AbstractType
{
    /**
     * @var Settings
     */
    private $settings;

    /**
     * SettingsType constructor.
     * @param Settings $settings
     */
    public function __construct(Settings $settings)
    {
        $this->settings = $settings;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach ($this->settings->keys() as $key) {
            $meta = $this->settings->getMeta($key);
            if ($meta['common']) {
                switch ($meta['type']) {
                    case Setting::TYPE_NUMBER:
                        $builder->add($key, NumberType::class, array(
                            'label' => $meta['display_name'],
                        ));
                        break;
                    case Setting::TYPE_BOOLEAN:
                        $builder->add($key, ChoiceType::class, array(
                            'label' => $meta['display_name'],
                            'choices' => array(
                                'Да' => true,
                                'Нет' => false,
                            )
                        ));
                        break;
                    default:
                        $builder->add($key, TextType::class, array(
                            'label' => $meta['display_name'],
                        ));
                        break;
                }
            }
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'attr' => array('novalidate' => 'novalidate'),
        ));
    }
}