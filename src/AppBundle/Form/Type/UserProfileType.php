<?php

namespace AppBundle\Form\Type;


use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserProfileType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Имя пользователя',
            ))
            ->add('yandex_money_account', TextType::class, array(
                'label' => 'Яндекс.Деньги',
            ))
            ->add('webmoney_account', TextType::class, array(
                'label' => 'WebMoney',
            ))
            ->add('qiwi_account', TextType::class, array(
                'label' => 'QIWI',
            ))
            ->add('setting_dashboard_display', ChoiceType::class, array(
                'label' => 'Отображение заявок',
                'choices' => array(
                    'Отображать заявки таблицей' => User::SETTING_DASHBOARD_DISPLAY_TABLE,
                    'Отображать заявки плиткой' => User::SETTING_DASHBOARD_DISPLAY_TILES,
                ),
            ))
            ->add('setting_show_tips', CheckboxType::class, array(
                'label' => 'Отображать советы',
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Сохранить',
            ))
        ;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
            'attr' => array('novalidate' => 'novalidate'),
        ));
    }
}