<?php

namespace AppBundle\Form\Type;


use AppBundle\Entity\Withdraw;
use AppBundle\Service\Settings;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Range;

class WithdrawType extends AbstractType
{
    /**
     * @var Settings
     */
    private $settings;

    /**
     * WithdrawType constructor.
     * @param Settings $settings
     */
    public function __construct(Settings $settings)
    {
        $this->settings = $settings;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, array(
                'choices' => array(
                    'Yandex.Money' => Withdraw::TYPE_YANDEX,
                    'WebMoney' => Withdraw::TYPE_WEBMONEY,
                    'QIWI' => Withdraw::TYPE_QIWI,
                    'Банковская карта' => Withdraw::TYPE_CARD
                ),
                'attr' => array(
                    'data-commissions' => json_encode(array(
                        Withdraw::TYPE_YANDEX => $this->settings->get('withdraw_commission_yandex'),
                        Withdraw::TYPE_WEBMONEY => $this->settings->get('withdraw_commission_webmoney'),
                        Withdraw::TYPE_QIWI => $this->settings->get('withdraw_commission_qiwi'),
                        Withdraw::TYPE_CARD => $this->settings->get('withdraw_commission_card'),
                    ))
                ),
                'label' => 'Способ оплаты',
            ))
            ->add('account', TextType::class, array(
                'label' => 'Номер счета/карты/телефона',
            ))
            ->add('amount', NumberType::class, array(
                'label' => 'Сумма',
                'constraints' => array(
                    new Range(array(
                        'min' => 1,
                        'minMessage' => 'Недопустимое значение суммы',
                        'max' => $builder->getData()->getUser()->getBalance()->getAmount(),
                        'maxMessage' => 'Сумма превышает ваш баланс',
                    )),
                )
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Запросить выплату',
            ))
        ;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Withdraw',
            'attr' => array('novalidate' => 'novalidate'),
        ));
    }
}