<?php
/**
 * Created by PhpStorm.
 * User: AlexZenter
 * Date: 07.06.2017
 * Time: 12:21
 */

namespace AppBundle\Security\Authentication\Token;


use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

class GoogleToken extends AbstractToken
{
    /**
     * @var string
     */
    protected $google_id;

    /**
     * @return string
     */
    public function getGoogleId()
    {
        return $this->google_id;
    }

    /**
     * @param string $google_id
     */
    public function setGoogleId($google_id)
    {
        $this->google_id = $google_id;
    }

    public function getCredentials()
    {
        return $this->google_id;
    }
}