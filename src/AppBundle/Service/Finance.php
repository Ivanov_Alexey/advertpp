<?php

namespace AppBundle\Service;


use AppBundle\Entity\Balance;
use AppBundle\Entity\Deposit;
use AppBundle\Entity\Order;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\Withdraw;

class Finance
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var Settings
     */
    protected $settings;

    /**
     * @var float
     */
    protected $commission_multiplier;

    /**
     * Finance constructor.
     * @param \Doctrine\Common\Persistence\ObjectManager $manager
     * @param Settings $settings
     */
    public function __construct(\Doctrine\Common\Persistence\ObjectManager $manager, Settings $settings)
    {
        $this->em = $manager;
        $this->settings = $settings;
    }

    /**
     * Посчитать стоимость лайка с учетом комиссии
     * @param float $price
     * @return float
     */
    public function getFullLikePrice($price)
    {
        return round($price * 1 + ($this->settings->get('like_commission') / 100), 2, PHP_ROUND_HALF_DOWN);
    }

    /**
     * @param Order $order
     * @return bool
     */
    public function createOrder($order)
    {
        // Обрабатываем изменения
        $balance = $order->getClient()->getBalance();
        $transaction = new Transaction();
        $transaction
            ->setAmount(-$order->getFullPrice())
            ->setBalance($balance)
            ->setOrder($order)
            ->setComment('Удердание средств для покрытия заявки')
            ->setCreatedAt(new \DateTime('now'));
        $balance->setAmount($balance->getAmount() + $transaction->getAmount());

        // Сохраняем изменения транзакцией
        $this->em->beginTransaction();
        try {
            $this->em->persist($order);
            $this->em->persist($transaction);
            $this->em->persist($balance);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            return false;
        }

        return true;
    }

    /**
     * @param Order $order
     * @param string $status
     * @return bool
     */
    public function processOrder($order, $status)
    {
        // Обрабатываем изменения
        $transaction = new Transaction();
        if ($status === Order::STATUS_DONE) {
            $balance = $order->getDoer()->getBalance();
            $transaction
                ->setOrder($order)
                ->setAmount($order->getPrice())
                ->setComment('Награда за выполнение заявки');
            // Комиссия уходит на баланс админа
            $admin_balance = $this->findAdminBalance();
            $admin_transaction = new Transaction();
            $admin_transaction
                ->setOrder($order)
                ->setBalance($admin_balance)
                ->setCreatedAt(new \DateTime('now'))
                ->setAmount($order->getFullPrice() - $order->getPrice())
                ->setComment('Комиссия по выполненной заявке');
            $admin_balance->setAmount($admin_balance->getAmount() + $admin_transaction->getAmount());
            $this->em->persist($admin_balance);
            $this->em->persist($admin_transaction);
        } else {
            $balance = $order->getClient()->getBalance();
            $transaction
                ->setOrder($order)
                ->setAmount($order->getFullPrice())
                ->setComment('Возвращение средств');
        }
        $order->setStatus($status)
            ->setClosedAt(new \DateTime('now'));
        $this->em->persist($order);
        $transaction
            ->setBalance($balance)
            ->setCreatedAt(new \DateTime('now'));
        $this->em->persist($transaction);
        $balance
            ->setAmount($balance->getAmount() + $transaction->getAmount());
        $this->em->persist($balance);

        // Сохраняем изменения транзакцией
        $this->em->beginTransaction();
        try {
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            return false;
        }

        return true;
    }

    /**
     * @param Withdraw $withdraw
     * @return bool
     */
    public function createWithdraw($withdraw)
    {
        // Обрабатываем изменения
        $balance = $withdraw->getUser()->getBalance();
        $transaction = new Transaction();
        $transaction
            ->setBalance($balance)
            ->setAmount(-$withdraw->getAmount())
            ->setComment('Удержание по запросу вывода средств')
            ->setCreatedAt(new \DateTime('now'));
        $balance->setAmount($balance->getAmount() + $transaction->getAmount());
        $withdraw
            ->setStatus(Withdraw::STATUS_PROCESSING)
            ->setCreatedAt(new \DateTime('now'));
        switch ($withdraw->getType()) {
            case Withdraw::TYPE_YANDEX:
                $withdraw->setPaymentAmount($this->subtractCommission($withdraw->getAmount(), $this->settings->get('withdraw_commission_yandex')));
                break;
            case Withdraw::TYPE_WEBMONEY:
                $withdraw->setPaymentAmount($this->subtractCommission($withdraw->getAmount(), $this->settings->get('withdraw_commission_webmoney')));
                break;
            case Withdraw::TYPE_QIWI:
                $withdraw->setPaymentAmount($this->subtractCommission($withdraw->getAmount(), $this->settings->get('withdraw_commission_qiwi')));
                break;
            case Withdraw::TYPE_CARD:
                $withdraw->setPaymentAmount($this->subtractCommission($withdraw->getAmount(), $this->settings->get('withdraw_commission_card')));
                break;
        }
        // Сохраняем изменения транзакцией
        $this->em->beginTransaction();
        try {
            $this->em->persist($withdraw);
            $this->em->persist($transaction);
            $this->em->persist($balance);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            return false;
        }

        return true;
    }

    /**
     * @param Withdraw $withdraw
     * @param string $status
     * @return bool
     */
    public function cancelWithdraw($withdraw, $status = Withdraw::STATUS_REJECTED)
    {
        if ($withdraw->getStatus() != Deposit::STATUS_PROCESSING) {
            return false;
        }

        // Обрабатываем изменения
        $balance = $withdraw->getUser()->getBalance();
        $transaction = new Transaction();
        $transaction
            ->setBalance($balance)
            ->setAmount($withdraw->getAmount())
            ->setCreatedAt(new \DateTime('now'));
        $balance->setAmount($balance->getAmount() + $transaction->getAmount());
        $withdraw
            ->setStatus($status)
            ->setUpdatedAt(new \DateTime('now'));
        if ($status == Withdraw::STATUS_REJECTED) {
            $transaction->setComment('Возвращение средств по отклоненной заявке на вывод средств');
        } else {
            $transaction->setComment('Возвращение средств по отмененной заявке на вывод средств');
        }
        // Сохраняем изменения транзакцией
        $this->em->beginTransaction();
        try {
            $this->em->persist($transaction);
            $this->em->persist($balance);
            $this->em->persist($withdraw);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            return false;
        }

        return true;
    }

    /**
     * @param Withdraw $withdraw
     * @return bool
     */
    public function processWithdraw($withdraw)
    {
        $withdraw
            ->setStatus(Withdraw::STATUS_DONE)
            ->setUpdatedAt(new \DateTime('now'));

        // Комиссия уходит на баланс админа
        $admin_balance = $this->findAdminBalance();
        $admin_transaction = new Transaction();
        $admin_transaction
            ->setBalance($admin_balance)
            ->setCreatedAt(new \DateTime('now'))
            ->setAmount($withdraw->getAmount() - $withdraw->getPaymentAmount())
            ->setComment('Комиссия за вывод средств');
        $admin_balance->setAmount($admin_balance->getAmount() + $admin_transaction->getAmount());

        // Сохраняем изменения транзакцией
        $this->em->beginTransaction();
        try {
            $this->em->persist($admin_balance);
            $this->em->persist($admin_transaction);
            $this->em->persist($withdraw);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            return false;
        }

        return true;
    }

    /**
     * @param Deposit $deposit
     */
    public function expireDeposit($deposit)
    {
        $deposit
            ->setStatus(Deposit::STATUS_FAIL)
            ->setUpdatedAt(new \DateTime('now'));
        $this->em->persist($deposit);
        $this->em->flush($deposit);
    }

    /**
     * @param Deposit $deposit
     * @return bool
     */
    public function confirmDeposit($deposit)
    {
        if ($deposit->getStatus() == Deposit::STATUS_DONE) {
            return false;
        }

        // Обрабатываем изменения
        $balance = $deposit->getUser()->getBalance();
        $transaction = new Transaction();
        $transaction
            ->setBalance($balance)
            ->setAmount($deposit->getAmount())
            ->setComment('Пополнение баланса')
            ->setCreatedAt(new \DateTime('now'));
        $balance->setAmount($balance->getAmount() + $transaction->getAmount());
        $deposit
            ->setStatus(Deposit::STATUS_DONE)
            ->setUpdatedAt(new \DateTime('now'));


        // Сохраняем изменения транзакцией
        $this->em->beginTransaction();
        try {
            $this->em->persist($transaction);
            $this->em->persist($balance);
            $this->em->persist($deposit);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            return false;
        }

        return true;
    }

    /**
     * @param Deposit $deposit
     * @return bool
     */
    public function cancelDeposit($deposit)
    {
        if ($deposit->getStatus() != Deposit::STATUS_DONE) {
            return false;
        }

        // Обрабатываем изменения
        $balance = $deposit->getUser()->getBalance();
        $transaction = new Transaction();
        $transaction
            ->setBalance($balance)
            ->setAmount(-$deposit->getAmount())
            ->setComment('Аннулирование пополнения баланса')
            ->setCreatedAt(new \DateTime('now'));
        $balance->setAmount($balance->getAmount() + $transaction->getAmount());
        $deposit
            ->setStatus(Deposit::STATUS_CANCELED)
            ->setUpdatedAt(new \DateTime('now'));

        // Сохраняем изменения транзакцией
        $this->em->beginTransaction();
        try {
            $this->em->persist($transaction);
            $this->em->persist($balance);
            $this->em->persist($deposit);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            return false;
        }

        return true;
    }

    /**
     * @return Balance
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function findAdminBalance()
    {
        $qb = $this->em->createQueryBuilder();
        $user = $qb->select(array('user', 'balance'))
            ->from('AppBundle:User', 'user')
            ->join('user.balance', 'balance')
            ->where('user.roles LIKE :role')
            ->setParameter('role', '%ROLE_ADMIN%')
            ->orderBy('user.id', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult();
        return $user->getBalance();
    }

    /**
     * @param $value
     * @param $percent
     * @return float
     */
    private function subtractCommission($value, $percent)
    {
        return round($value * (100 - $percent) / 100, 2, PHP_ROUND_HALF_DOWN);
    }
}