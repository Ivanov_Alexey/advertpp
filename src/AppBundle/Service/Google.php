<?php

namespace AppBundle\Service;


use AppBundle\Entity\Balance;
use AppBundle\Entity\GoogleAccount;
use AppBundle\Entity\User;
use AppBundle\Security\Authentication\Token\GoogleToken;
use Doctrine\ORM\EntityManager;
use HappyR\Google\ApiBundle\Services\GoogleClient;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class Google
{
    const MAX_VIDEO_RESULTS = 6;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var GoogleClient
     */
    protected $client;
    
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var GoogleAccount
     */
    protected $current_google_account;

    /**
     * Google constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->client = $container->get('happyr.google.api.client');
        $this->em = $container->get('doctrine.orm.entity_manager');
    }

    /**
     * Check if user has google access
     * @return bool
     */
    public function isAuthenticated()
    {
        // Если пользователь по каким то причинам вышел
        if ($this->container->get('security.token_storage')->getToken()->getUser() === null) {
            return false;
        }
        // Загружаем текущий google аккаунт
        if ($this->current_google_account === null) {
            $current_google_account_id = $this->container->get('session')->get('current_google_account');
            if (!empty($current_google_account_id)) {
                $this->current_google_account = $this->em->getRepository('AppBundle:GoogleAccount')
                    ->findOneBy(array('id' => $current_google_account_id));
                if (!($this->current_google_account instanceof GoogleAccount)) {
                    return false;
                }
            } else {
                return false;
            }
        }
        // Проверяем токен доступа
        $this->client->getGoogleClient()->setAccessToken($this->current_google_account->getToken());
        if ($this->client->getGoogleClient()->isAccessTokenExpired()) {
            $this->client->getGoogleClient()->refreshToken(
                $this->client->getGoogleClient()->getRefreshToken()
            );
        }
        return !$this->client->getGoogleClient()->isAccessTokenExpired();
    }

    /**
     * @param array $access_token
     * @return User
     */
    public function authenticate($access_token)
    {
        $user_needs_authentication = false;
        $plus_service = new \Google_Service_Plus($this->client->getGoogleClient());
        $account_info = $plus_service->people->get('me');

        /** @var User $user */
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $google_account = $this->em->getRepository('AppBundle:GoogleAccount')
            ->findOneBy(array('google_id' => $account_info->getId()));

        // Если аккаунт уже сохранен - просто обновляем токен
        if ($google_account instanceof GoogleAccount) {
            // Если не тот пользователь
            if ($user instanceof User && $google_account->getUser()->getId() != $user->getId()) {
                return array(
                    'status' => 'error',
                    'message' => 'Аккаунт Youtube ипользуется другим пользователем. Выполните выход, затем попробуйте еще раз.',
                );
            } else {
                $google_account->setToken($access_token);
                if (!($user instanceof User)) {
                    $user = $google_account->getUser();
                    $user_needs_authentication = true;
                }
                $result = array(
                    'status' => 'success',
                    'message' => '',
                );
            }
        // Иначе создаем новый аккаунт
        } else {
            $google_account = new GoogleAccount();
            $google_account
                ->setToken($access_token)
                ->setGoogleId($account_info->getId());
            // Если пользователь залогинен - прикрепляем аккаунт у нему
            if ($user instanceof User) {
                $google_account->setUser($user);
                $result = array(
                    'status' => 'success',
                    'message' => 'Аккаунт Youtube успено привязан',
                );
            // Иначе пытаемся зарегистрировать нового пользователя
            } else if (count($account_info->getEmails())) {
                // Создание пользователя
                $user = new User();
                $user
                    ->addGoogleAccount($google_account)
                    ->setEmail($account_info->getEmails()[0]->getValue())
                    ->setName($account_info->getDisplayName())
                    ->setPlainPassword(substr(md5(random_bytes(10)), 0, 8));
                $user->setPassword($this->container->get('security.password_encoder')->encodePassword($user, $user->getPlainPassword()));
                // Создание баланса
                $balance = new Balance();
                $balance->setUser($user);
                $balance->setAmount(0);

                $google_account->setUser($user);
                $this->em->persist($user);
                $this->em->persist($balance);
                $user_needs_authentication = true;

                $result = array(
                    'status' => 'success',
                    'message' => '', //Аккаунт Youtube успено привязан к новому пользователю. Логина для входа: '.$user->getEmail().', пароль: '.$user->getPlainPassword().'.',
                    'new_user' => $user,
                );
            } else {
                return array(
                    'status' => 'error',
                    'message' => 'Не удлось идентифицировать пользователя.',
                );
            }
        }
        $this->em->persist($google_account);
        $this->em->flush();

        // Если требуется залогинить пользователя
        if ($user_needs_authentication) {
            $this->authenticateWithGoogleAccount($user, $google_account);
        }
        //Обновляем информацию о youtube аккаунте
        $this->current_google_account = $google_account;
        $this->updateYoutubeChannel();

        return $result;
    }

    /**
     * @return GoogleAccount
     */
    public function getCurrentGoogleAccount()
    {
        if ($this->isAuthenticated()) {
            return $this->current_google_account;
        }

        return null;
    }

    /**
     * @param User $user
     * @param GoogleAccount $google_account
     */
    private function authenticateWithGoogleAccount($user, $google_account)
    {
        // Создаем токен для доступа с правами пользователя
        $token = new GoogleToken($user->getRoles());
        $token->setGoogleId($google_account->getGoogleId());
        $token->setUser($user);
        $token->setAuthenticated(true);
        $this->container->get('security.token_storage')->setToken($token);
        // Устанавливаем текущим аккаунтом тот, через который был выполнен вход
        $this->container->get('session')->set('current_google_account', $google_account->getId());
    }
    /**
     * Like video
     * @param string $video_id
     * @return bool
     */
    public function like($video_id)
    {
        if ($this->isAuthenticated()) {
            $service = new \Google_Service_YouTube($this->client->getGoogleClient());
            try {
                /** @var \GuzzleHttp\Psr7\Response $response */
                $response = $service->videos->rate($video_id, 'like');
                if ($response->getStatusCode() >= 300) {
                    return false;
                }
                return true;
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
    }

    /**
     * Загрузить список видео на канале пользователя
     * @param string $page_token
     * @return bool|\Google_Service_YouTube_PlaylistItemListResponse
     * TODO обрабатывать видео и выдавать только нужную инфомрацию
     */
    public function videoList($page_token = null)
    {
        if ($this->isAuthenticated()) {
            // Если еще нет информации о плейлисте с загрузками - обновим информацию
            if (empty($this->current_google_account->getUploadsPlaylistId())) {
                $this->updateYoutubeChannel();
            }
            // Загружаем список видео
            $service = new \Google_Service_YouTube($this->client->getGoogleClient());
            $videos = $service->playlistItems->listPlaylistItems(
                'snippet',
                array(
                    'maxResults' => self::MAX_VIDEO_RESULTS,
                    'pageToken' => $page_token,
                    'playlistId' => $this->current_google_account->getUploadsPlaylistId()
                )
            );
            return $videos;
        }
        return false;
    }

    /**
     * Обновить информацию о youtube канале пользователя
     * @param GoogleAccount|null $account
     * @return bool
     */
    public function updateYoutubeChannel($account = null)
    {
        if ($account instanceof GoogleAccount) {
            $this->client->getGoogleClient()->setAccessToken($account->getToken());
            if ($this->client->getGoogleClient()->isAccessTokenExpired()) {
                $this->client->getGoogleClient()->refreshToken(
                    $this->client->getGoogleClient()->getRefreshToken()
                );
            }
            if ($this->client->getGoogleClient()->isAccessTokenExpired()) {
                return false;
            }
        } else if ($this->isAuthenticated()) {
            $account = $this->current_google_account;
        } else {
            return false;
        }

        $service = new \Google_Service_YouTube($this->client->getGoogleClient());
        $response = $service->channels->listChannels('contentDetails,statistics,snippet', array('mine' => true));
        if ($response->count() > 0) {
            /** @var \Google_Service_YouTube_Channel $channel */
            $channel = $response->getItems()[0];
            $account
                ->setChannelId($channel->getId())
                ->setUploadsPlaylistId($channel->getContentDetails()->getRelatedPlaylists()->getUploads())
                ->setChannelStats((array) $channel->getStatistics())
                ->setChannelName($channel->getSnippet()->getTitle())
                ->setChannelImages(array(
                    'default' =>  $channel->getSnippet()->getThumbnails()->getDefault()->getUrl(),
                    'medium' =>  $channel->getSnippet()->getThumbnails()->getMedium()->getUrl(),
                    'high' =>  $channel->getSnippet()->getThumbnails()->getHigh()->getUrl(),
                ));
            $this->em->persist($account);
            $this->em->flush();

            return true;
        }

        return false;
    }
}