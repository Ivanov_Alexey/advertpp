<?php

namespace AppBundle\Service\Notification;


interface NotifiableInterface
{
    public function getId();
}