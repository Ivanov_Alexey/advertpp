<?php

namespace AppBundle\Service\Notification;


use AppBundle\Entity\Notification;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;

class NotificationManager
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * NotificationManager constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param User $user
     * @param NotifiableInterface $notifiable
     * @param null $message
     */
    public function push(User $user, NotifiableInterface $notifiable, $message = null)
    {
        $notification = new Notification();
        $notification
            ->setSource(get_class($notifiable))
            ->setSourceId($notifiable->getId())
            ->setText($message)
            ->setUser($user)
            ->setCreatedAt(new \DateTime());
        $this->em->persist($notification);
        $this->em->flush();
    }

    /**
     * @param User $user
     * @param $source
     * @return int
     */
    public function count(User $user, $source)
    {
        return $this->em->getRepository('AppBundle:Notification')
            ->countByUserAndSource($user, $source);
    }

    /**
     * @param User $user
     * @param NotifiableInterface $notifiable
     * @return bool
     */
    public function pop(User $user, NotifiableInterface $notifiable)
    {
        $notifications = $this->em->getRepository('AppBundle:Notification')
            ->findByUserAndSourceID($user, $notifiable);
        if (count($notifications)) {
            foreach ($notifications as $notification) {
                $this->em->remove($notification);
            }
            $this->em->flush();
            return true;
        }
        return false;
    }
}