<?php

namespace AppBundle\Service;


use AppBundle\Entity\Setting;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;

class Settings
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var array
     */
    protected $meta;

    /**
     * Settings constructor.
     * @param ObjectManager $manager
     */
    public function __construct(ObjectManager $manager)
    {
        $this->em = $manager;
    }

    /**
     * @param string $key
     * @param mixed $default
     * @return string
     */
    public function get($key, $default = null) 
    {
        $this->load();
        if (isset($this->data[$key])) {
            return $this->data[$key];
        } else {
            return $default;
        }
    }

    /**
     * @return array
     */
    public function getAll()
    {
        $this->load();
        return $this->data;
    }

    /**
     * @param $key
     * @return array
     */
    public function getMeta($key)
    {
        return $this->meta[$key];
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function set($key, $value)
    {
        $setting = $this->em
            ->getRepository('AppBundle:Setting')
            ->findOneBy(array('name' => $key));
        if (!($setting instanceof Setting)) {
            $setting = new Setting();
            $setting->setName($key);
        }
        $setting->setValue($value);
        $this->em->persist($setting);
        $this->em->flush();
    }

    /**
     * @param $data
     * TODO
     */
    public function setMany($data)
    {
        foreach ($data as $key => $value) {
            $this->set($key, $value);
        }
    }

    /**
     * Available settings keys
     * @return array
     */
    public function keys()
    {
        $this->load();
        return array_keys($this->data);
    }

    /**
     * Load settings from database
     */
    private function load()
    {
        if ($this->data === null) {
            $settings = $this->em
                ->getRepository('AppBundle:Setting')
                ->findAll();
            $this->data = array();
            $this->meta = array();
            /** @var Setting $setting */
            foreach ($settings as $setting) {
                switch ($setting->getType()) {
                    case Setting::TYPE_NUMBER:
                        $this->data[$setting->getName()] = floatval($setting->getValue());
                        break;
                    case Setting::TYPE_BOOLEAN:
                        $this->data[$setting->getName()] = filter_var($setting->getValue(), FILTER_VALIDATE_BOOLEAN);
                        break;
                    default:
                        $this->data[$setting->getName()] = $setting->getValue();
                }
                $this->meta[$setting->getName()] = array(
                    'type' => $setting->getType(),
                    'common' => $setting->getCommon(),
                    'display_name' => $setting->getDisplayName(),
                );
            }
        }
    }
}