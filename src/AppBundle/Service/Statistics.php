<?php

namespace AppBundle\Service;


use AppBundle\Entity\Order;

class Statistics
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * Finance constructor.
     * @param \Doctrine\Common\Persistence\ObjectManager $manager
     */
    public function __construct(\Doctrine\Common\Persistence\ObjectManager $manager)
    {
        $this->em = $manager;
    }

    /**
     * @return mixed
     */
    public function totalBalances()
    {
        return $this->em->createQueryBuilder()
            ->select('sum(balance.amount)')
            ->from('AppBundle:Balance', 'balance')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function ordersValue($days)
    {
        return $this->em->createQueryBuilder()
            ->select('sum(o.full_price)')
            ->from('AppBundle:Order', 'o')
            ->where('o.closed_at > :start')
            ->andWhere('o.status = :status')
            ->setParameter('start', new \DateTime('-'.$days.'day'))
            ->setParameter('status', Order::STATUS_DONE)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function ordersCount($days, $status = null)
    {
        $qb = $this->em->createQueryBuilder()
            ->select('count(o)')
            ->from('AppBundle:Order', 'o')
            ->where('o.closed_at > :start')
            ->setParameter('start', new \DateTime('-'.$days.'day'));
        if ($status !== null) {
            $qb->andWhere('o.status = :status');
            $qb->setParameter('status', $status);
        }
        return $qb->getQuery()
            ->getSingleScalarResult();
    }
}