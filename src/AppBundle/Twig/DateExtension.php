<?php

namespace AppBundle\Twig;


class DateExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('simple_date', array($this, 'simpleDateFilter')),
        );
    }

    public function simpleDateFilter(\DateTime $date)
    {
        $only_date = clone $date;
        $only_date->setTime(0,0,0);
        $days_diff = $only_date->diff(new \Datetime('today'));
        switch ($days_diff->days) {
            case 0:
                $now_diff = $date->diff(new \DateTime());
                $minutes = $now_diff->days * 24 * 60 + $now_diff->h * 60 + $now_diff->i;
                if ($minutes < 3) {
                    return 'Только что';
                } else {
                    return 'Сегодня';
                }
            case -1:
                return 'Вчера';
            default:
                return $date->format('d.m.Y H:i');
        }
    }
}